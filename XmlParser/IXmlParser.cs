﻿using System;
using XmlParser.Dtos;

namespace XmlParser
{
    interface IXmlParser
    {
        /// <summary>
        /// Serializes a message object to an xml form
        /// </summary>
        /// <param name="newMessage">
        /// Object of one of types from XmlParser.Dtos namespace:
        /// - error
        /// - playerLogin
        /// - gameMasterLogin
        /// - loginResponse
        /// - beginGame
        /// - gameState
        /// - move
        /// - thank you
        /// - leaveGame
        /// - playerLeftGame
        /// - logout
        /// - serverShutdown
        /// - championsList
        /// </param>
        /// <returns>returns a string containing xml form of newMessage object</returns>
        string Serialize(DtoMessage newMessage);


        /// <summary>
        /// Deserializes xml string to an object;
        /// The type of object will be the same sa attribute type i message xml
        /// </summary>
        /// <remarks>
        /// message type contains a true type name of returned object;
        /// For example:
        /// DtoMessage mes = Deserialize(string xml);
        /// if(mes.Type == "leaveGame")
        ///     leaveGame receivedMessage = (leaveGame)mes;
        /// </remarks>
        /// <param name="xml">xml string to deserialize</param>
        /// <returns>a new message object initialized by information from xml</returns>
        DtoMessage Deserialize(string xml);

        /// <summary>
        /// Deserialize xml to an object of type T;
        /// This method is used to deserialize xml content which depends on game type;
        /// Using normal Deserialize method when message of type "move" 
        /// (independent from a type of a game) is reveived will produce object of type
        /// move which doesn't contain information about move. Invoking this method with
        /// T of type created esspecially for concrete game will produce object of this
        /// type with all desired information;
        /// </summary>
        /// <param name="xml">xml string to deserialize</param>
        /// <returns>a new object of type T initialized by information from xml</returns>
        T Deserialize<T>(string xml);
    }
}
