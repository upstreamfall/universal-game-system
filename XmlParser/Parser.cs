﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XmlParser.Dtos;
using YAXLib;

namespace XmlParser
{
    /// <summary>
    /// Class implementing IXmlParser interface which has methods to deserialize xml strings
    /// to Dto objects and to serialize them back to xml strings.
    /// </summary>
    /// <exception cref="ParserException">
    /// Methods from Parser class throw ParserException with information about an error;
    /// </exception>
    public class Parser : IXmlParser
    {
        public string Serialize(DtoMessage newMessage)
        {
            string result = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\r\n";
            Type messageType = newMessage.GetType();
            try
            {
                YAXSerializer serializer = new YAXSerializer(messageType);
                result += serializer.Serialize(newMessage);
            }
            catch (YAXException ex)
            {
                throw new ParserException("Exception occured in parser library (YAXlib): \n" + ex.Message);
            }
            catch (Exception ex)
            {
                throw new ParserException("Parser inner exception occured: \n" + ex.Message);
            }
            return result;
        }

        public DtoMessage Deserialize(string xml)
        {
            DtoMessage result = null;        
            try
            {
                Type type = GetMessageType(xml);
                YAXSerializer ser = new YAXSerializer(type);
                result = (DtoMessage)ser.Deserialize(xml);
            }
            catch (YAXException ex)
            {
                throw new ParserException("Exception occured in parser library (YAXlib): \n" + ex.Message);
            }
            catch (Exception ex)
            {
                throw new ParserException("Parser inner exception occured: \n" + ex.Message);
            }
            return result;
        }

        public T Deserialize<T>(string xml)
        {
            T result = default(T);
            try
            {
                YAXSerializer ser = new YAXSerializer(typeof(T));
                result = (T)ser.Deserialize(xml);
            }
            catch (YAXException ex)
            {
                throw new ParserException("Exception occured in parser library (YAXlib): \n" + ex.Message);
            }
            catch (Exception ex)
            {
                throw new ParserException("Parser inner exception occured: \n" + ex.Message);
            }
            return result;
        }

        private Type GetMessageType(string xml)
        {
            Type result = typeof(DtoMessage);
            String typeName = "XmlParser.Dtos.Dto";
            YAXSerializer serializer = new YAXSerializer(result);
            DtoMessage mes = (DtoMessage)serializer.Deserialize(xml);
            typeName += UppercaseFirst(mes.Type.Trim().Replace(" ", string.Empty));
            result = Type.GetType(typeName);
            if (result == null && mes != null)
                result = typeof(DtoMessage);
            else if (result == null)
                throw new ParserException("Can't find any DTO which suits this xml");
            return result;
        }

        private string UppercaseFirst(string s)
        {
            return char.ToUpper(s[0]) + s.Substring(1);
        }

    }
}
