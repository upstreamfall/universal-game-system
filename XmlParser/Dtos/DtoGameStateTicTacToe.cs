﻿using System.Collections.Generic;
using YAXLib;

namespace XmlParser.Dtos
{
    [YAXSerializeAs("message")]
    public class DtoGameStateTicTacToe : DtoGameState
    {
        [YAXSerializeAs("gameState")]
        public Tac Move { get; set; }

        public DtoGameStateTicTacToe() : base() { Move = new Tac(); }
        public DtoGameStateTicTacToe(string gameId, string nextPlayer)
            :base(gameId, nextPlayer)
        {
            Move = new Tac();
        }
        public DtoGameStateTicTacToe(string gameId, string nextPlayer, Tac move)
            : base(gameId, nextPlayer)
        {
            Move = move;
        }
        public DtoGameStateTicTacToe(string gameId, List<PlayerResult> results)
            : base(gameId, results) { Move = new Tac(); }
    }

    [YAXSerializeAs("tac")]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects)]
    public class Tac
    {
        [YAXSerializeAs("x")]
        [YAXAttributeFor("tac")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore, DefaultValue = null)]
        public int? X { get; set; }

        [YAXSerializeAs("y")]
        [YAXAttributeFor("tac")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore, DefaultValue = null)]
        public int? Y { get; set; }

        public Tac() 
        {
            X = null;
            Y = null;
        }
        public Tac(int? x, int? y)
        {
            X = x;
            Y = y;
        }
    }
}
