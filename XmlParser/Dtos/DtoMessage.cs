﻿using YAXLib;

namespace XmlParser.Dtos
{
    [YAXSerializeAs("message")]
    public class DtoMessage
    {
        [YAXAttributeFor(".")]
        [YAXSerializeAs("type")]
        public string Type { get; set; }

        public DtoMessage() { }
        public DtoMessage(string type)
        {
            Type = type;
        }

        //public override string ToString()
        //{
        //    string print = "Message from Dtomessage";
        //    return print;
        //}
    }
}
