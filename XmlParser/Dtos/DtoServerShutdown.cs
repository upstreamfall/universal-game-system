﻿using YAXLib;

namespace XmlParser.Dtos 
{
    [YAXSerializeAs("message")]
    public class DtoServerShutdown: DtoMessage
    {
        public DtoServerShutdown() : base("serverShutdown") { }
    }
}
