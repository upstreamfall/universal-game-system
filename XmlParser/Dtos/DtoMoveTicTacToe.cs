﻿using YAXLib;

namespace XmlParser.Dtos
{
    [YAXSerializeAs("message")]
    public class DtoMoveTicTacToe : DtoMove
    {
        [YAXSerializeAs("move")]
        public Tic Move { get; set; }

        public DtoMoveTicTacToe() : base() { }
        public DtoMoveTicTacToe(string gameId, Tic move)
            : base(gameId)
        {
            Move = move;
        }

        public override string ToString()
        {
            string print = "DtoTicTacToe: " + base.ToString() + "; move:" + Move.ToString() + ";";
            return base.ToString();
        }
    }

    public class Tic
    {
        [YAXSerializeAs("x")]
        [YAXAttributeFor("tic")]
        public int X { get; set; }

        [YAXSerializeAs("y")]
        [YAXAttributeFor("tic")]
        public int Y { get; set; }

        public Tic() { }
        public Tic(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override string ToString()
        {
            string print = "Tic: x:" + X + "; y:" + Y + ";";
            return print;
        }
    }
}
