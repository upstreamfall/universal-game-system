﻿using YAXLib;

namespace XmlParser.Dtos
{
    [YAXSerializeAs("message")]
    public class DtoLogout : DtoMessage
    {
        public DtoLogout() : base("logout") { }
    }
}
