﻿using YAXLib;

namespace XmlParser.Dtos
{
    [YAXSerializeAs("message")]
    public class DtoMove : DtoMessage
    {
        [YAXAttributeFor("gameId")]
        [YAXSerializeAs("id")]
        public string GameId { get; set; }

        public DtoMove() : base("move") { }
        public DtoMove(string gameId)
            : base("move")
        {
            GameId = gameId;
        }

        public override string ToString()
        {
            string print = "Move: gameID:" + GameId + ";";
            return print;
        }
    }
}