﻿using System.Collections.Generic;
using YAXLib;

namespace XmlParser.Dtos
{
    [YAXSerializeAs("message")]
    [YAXSerializableType(Options=YAXSerializationOptions.DontSerializeNullObjects)]
    public class DtoGameState : DtoMessage
    {
        [YAXAttributeFor("gameId")]
        [YAXSerializeAs("id")]
        public string GameId { get; set; }

        [YAXAttributeFor("nextPlayer")]
        [YAXSerializeAs("nick")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore, DefaultValue = null)]
        public string NextPlayerNick { get; set; }

        [YAXSerializeAs("gameOver")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore, DefaultValue=null)]
        [YAXCollectionAttribute(YAXCollectionSerializationTypes.Serially, EachElementName = "player")]
        public List<PlayerResult> ResultsOfPlayers { get; set; }

        public DtoGameState() : base("gameState") { }
        public DtoGameState(string gameId, string nextPlayer)
            : base("gameState")
        {
            GameId = gameId;
            NextPlayerNick = nextPlayer;
            ResultsOfPlayers = null;
        }
        public DtoGameState(string gameId, List<PlayerResult> results)
            : base("gameState")
        {
            GameId = gameId;
            NextPlayerNick = null;
            ResultsOfPlayers = results;
        }
    }

    //[YAXSerializeAs("player")]
    public class PlayerResult
    {
        [YAXAttributeFor(".")]
        [YAXSerializeAs("nick")]
        public string Nick { get; set; }

        [YAXAttributeFor(".")]
        [YAXSerializeAs("result")]
        public string Result { get; set; }

        public PlayerResult() { }
        public PlayerResult(string nick, string result)
        {
            Nick = nick;
            Result = result;
        }
    }
}
