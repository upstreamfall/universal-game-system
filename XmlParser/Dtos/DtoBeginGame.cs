﻿using System.Collections.Generic;
using YAXLib;

namespace XmlParser.Dtos
{
    [YAXSerializeAs("message")]
    public class DtoBeginGame : DtoMessage
    {
        [YAXAttributeFor("gameId")]
        [YAXSerializeAs("id")]
        public string GameId { get; set; }

        [YAXCollectionAttribute(YAXCollectionSerializationTypes.RecursiveWithNoContainingElement, EachElementName="player")]
        public List<DtoPlayer> Players { get; set; }

        public DtoBeginGame() : base("beginGame") { }
        public DtoBeginGame(string gameId, List<DtoPlayer> players)
            : base("beginGame")
        {
            GameId = gameId;
            Players = players;
        }

        public override string ToString()
        {
            string players = string.Join(", ", Players);
            string print = "BeginGame: gameID:" + GameId + "; playerList:" + players + ";";
            return print;
        }
    }
}
