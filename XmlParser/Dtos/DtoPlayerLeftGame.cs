﻿using YAXLib;

namespace XmlParser.Dtos
{
    [YAXSerializeAs("message")]
    public class DtoPlayerLeftGame : DtoMessage
    {
        [YAXSerializeAs("nick")]
        [YAXAttributeFor("player")]
        public string Nick { get; set; }

        [YAXAttributeFor("gameId")]
        [YAXSerializeAs("id")]
        public string GameId { get; set; }

        public DtoPlayerLeftGame() : base("playerLeftGame") { }
        public DtoPlayerLeftGame(string gameId, string nick)
            : base("playerLeftGame")
        {
            GameId = gameId;
            Nick = nick;
        }

        public override string ToString()
        {
            string print = "Player left game: gameID: " + GameId + "; nick:" + Nick + ";";
            return print;
        }
    }
}
