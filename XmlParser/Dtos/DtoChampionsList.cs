﻿using System.Collections.Generic;
using System.Linq;
using YAXLib;

namespace XmlParser.Dtos
{
    [YAXSerializeAs("message")]
    public class DtoChampionsList : DtoMessage
    {
        [YAXCollectionAttribute(YAXCollectionSerializationTypes.RecursiveWithNoContainingElement, EachElementName = "player")]
        public List<PlayerResultChampionship> PlayerChampionshipResults { get; set; }

        public DtoChampionsList() : base("championsList") { }
        public DtoChampionsList(List<PlayerResultChampionship> playerChampionshipResults)
            : base("championsList")
        {
            PlayerChampionshipResults = playerChampionshipResults.OrderByDescending(t => t.Won).ToList();
        }
    }
    
    //[YAXSerializeAs("player")]
    public class PlayerResultChampionship
    {
        [YAXAttributeFor(".")]
        [YAXSerializeAs("nick")]
        public string Nick { get; set; }

        [YAXAttributeFor(".")]
        [YAXSerializeAs("won")]
        public int Won { get; set; }

        [YAXAttributeFor(".")]
        [YAXSerializeAs("lost")]
        public int Lost { get; set; }

        public PlayerResultChampionship() { }
        public PlayerResultChampionship(string nick, int won, int lost)
        {
            Nick = nick;
            Won = won;
            Lost = lost;
        }
    }
}
