﻿using YAXLib;

namespace XmlParser.Dtos
{
    [YAXSerializeAs("message")]
    public class DtoError : DtoMessage
    {
        [YAXValueFor(".")]
        public string ErrorMessage { get; set; }

        public DtoError() : base("error") { }
        public DtoError(string errorMessage)
            : base("error")
        {
            ErrorMessage = errorMessage;
        }
    }
}
