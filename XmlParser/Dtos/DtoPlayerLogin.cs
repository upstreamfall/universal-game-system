﻿using YAXLib;

namespace XmlParser.Dtos
{
    [YAXSerializeAs("message")]
    public class DtoPlayerLogin : DtoMessage
    {
        [YAXAttributeFor("playerLogin")]
        [YAXSerializeAs("nick")]
        public string Nick { get; set; }

        [YAXAttributeFor("playerLogin")]
        [YAXSerializeAs("gameType")]
        public string GameType { get; set; }

        public DtoPlayerLogin() : base("playerLogin") { }
        public DtoPlayerLogin(string nick, string gameType)
            : base("playerLogin")
        {
            Nick = nick;
            GameType = gameType;
        }
    }
}
