﻿using YAXLib;

namespace XmlParser.Dtos
{
    [YAXSerializeAs("message")]
    public class DtoThankyou : DtoMessage
    {
        [YAXAttributeFor("gameId")]
        [YAXSerializeAs("id")]
        public string GameId { get; set; }

        public DtoThankyou() : base("thank you") { }
        public DtoThankyou(string gameId)
            : base("thank you")
        {
            GameId = gameId;
        }
    }
}
