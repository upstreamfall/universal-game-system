﻿using YAXLib;

namespace XmlParser.Dtos
{
    [YAXSerializeAs("message")]
    public class DtoGameMasterLogin : DtoMessage
    {
        [YAXSerializeAs("id")]
        [YAXAttributeFor("gameMasterLogin")]
        public string Id { get; set; }

        [YAXSerializeAs("gameType")]
        [YAXAttributeFor("gameMasterLogin")]
        public string GameType { get; set; }

        [YAXSerializeAs("playersMin")]
        [YAXAttributeFor("gameMasterLogin")]
        public int PlayersMin { get; set; }

        [YAXSerializeAs("playersMax")]
        [YAXAttributeFor("gameMasterLogin")]
        public int PlayersMax { get; set; }

        public DtoGameMasterLogin() : base("gameMasterLogin") { }
        public DtoGameMasterLogin(string id, string gameType, int playersMin, int playersMax)
            : base("gameMasterLogin")
        {
            Id = id;
            GameType = gameType;
            if (playersMin <= 1)
                throw new ParserException("Players min can't be less than 2");
            if (playersMax < playersMin)
                throw new ParserException("Players max can't be less than players min");
            PlayersMin = playersMin;
            PlayersMax = playersMax;
        }

        public override string ToString()
        {
            return "Id:" + Id + "; gameType:" + GameType + "; playerMin:" + PlayersMin + "; playerMax:" + PlayersMax + ";"; 
        }
    }
}
