﻿using YAXLib;

namespace XmlParser.Dtos
{
    [YAXSerializeAs("message")]
    [YAXSerializableType(Options = YAXSerializationOptions.DontSerializeNullObjects)]
    public class DtoLoginResponse : DtoMessage
    {
        [YAXSerializeAs("accept")]
        [YAXAttributeFor("response")]
        public string Accept { get; set; }
        
        [YAXSerializeAs("id")]
        [YAXAttributeFor("error")]
        [YAXErrorIfMissed(YAXExceptionTypes.Ignore, DefaultValue=null)]
        public int? ErrorId { get; set; }

        public DtoLoginResponse() : base("loginResponse") { }
        public DtoLoginResponse(string accepted)
            : base("loginResponse")
        {
            if (!accepted.Equals("yes") && !accepted.Equals("no"))
                throw new ParserException("Error when creating DtoLoginResponse object - wrong accepted parameter!");
            Accept = accepted;
            ErrorId = null;
        }
        public DtoLoginResponse(string accepted, int errorId = 0)
            : base("loginResponse")
        {
            if (!accepted.Equals("yes") && !accepted.Equals("no"))
                throw new ParserException("Error when creating DtoLoginResponse object - wrong accepted parameter!");
            if (accepted.Equals("yes") && errorId != 0)
                throw new ParserException("Error when creating DtoLoginResponse object - errorId is set when accepted = yes!");
            Accept = accepted;
            ErrorId = errorId;
        }

        public override string ToString()
        {
            return "Response:" + Accept + ";" + ( ErrorId != null ? " ErrorId:" + ErrorId : "" );
        }
    }
}
