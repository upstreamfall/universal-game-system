﻿using YAXLib;

namespace XmlParser.Dtos
{
    public class DtoPlayer
    {
        [YAXSerializeAs("nick")]
        [YAXAttributeFor(".")]
        public string Nick { get; set; }

        public DtoPlayer() { }
        public DtoPlayer(string nick)
        {
            Nick = nick;
        }

        public override string ToString()
        {
            return Nick;
        }
    }
}
