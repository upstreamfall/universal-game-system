﻿using YAXLib;

namespace XmlParser.Dtos
{
    [YAXSerializeAs("message")]
    public class DtoLeaveGame : DtoMessage
    {
        [YAXAttributeFor("gameId")]
        [YAXSerializeAs("id")]
        public string GameId { get; set; }

        public DtoLeaveGame() : base("leaveGame") { }
        public DtoLeaveGame(string gameId)
            : base("leaveGame")
        {
            GameId = gameId;
        }
    }
}
