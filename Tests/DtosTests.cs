﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using XmlParser;
using XmlParser.Dtos;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace UnitTestsForXmlParser
{
    [TestClass]
    public class DtosTests
    {
        Parser testParser;

        [TestInitialize]
        public void Initialize()
        {
            testParser = new Parser();
        }

        [TestMethod]
        public void DtoBeginGameTestMethod()
        {
            string expectedXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><message type=\"beginGame\">  <gameId id=\"test\" />  <player nick=\"test1\" />  <player nick=\"test2\" />  <player nick=\"test3\" /></message>";
            DtoBeginGame beginGameDto = new DtoBeginGame("test", new List<DtoPlayer> { new DtoPlayer("test1"), new DtoPlayer("test2"), new DtoPlayer("test3") });
            string result = testParser.Serialize(beginGameDto);
            DtoBeginGame result2 = (DtoBeginGame)testParser.Deserialize(result);
            result = Regex.Replace(result, @"\r\n", "");
            Assert.AreEqual<string>(expectedXml, result);
            Assert.AreEqual(beginGameDto.GameId, result2.GameId);
            Assert.AreEqual(beginGameDto.Type, result2.Type);
            for(int i=0; i<3; i++)
                Assert.AreEqual(beginGameDto.Players[i].Nick, result2.Players[i].Nick);
        }

        [TestMethod]
        public void DtoChampionsListTestMethod()
        {
            string expectedXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><message type=\"championsList\">  <player nick=\"test2\" won=\"2\" lost=\"0\" />  <player nick=\"test1\" won=\"0\" lost=\"2\" /></message>";
            DtoChampionsList championsListDto = new DtoChampionsList(new List<PlayerResultChampionship> { new PlayerResultChampionship("test1", 0, 2), new PlayerResultChampionship("test2", 2, 0) });
            string result = testParser.Serialize(championsListDto);
            DtoChampionsList result2 = (DtoChampionsList)testParser.Deserialize(result);
            result = Regex.Replace(result, @"\r\n", "");
            Assert.AreEqual<string>(expectedXml, result);
            Assert.AreEqual(championsListDto.Type, result2.Type);
            Assert.AreEqual(championsListDto.PlayerChampionshipResults[0].Nick, result2.PlayerChampionshipResults[0].Nick);
            Assert.AreEqual(championsListDto.PlayerChampionshipResults[1].Nick, result2.PlayerChampionshipResults[1].Nick);
            Assert.AreEqual(championsListDto.PlayerChampionshipResults[0].Lost, result2.PlayerChampionshipResults[0].Lost);
            Assert.AreEqual(championsListDto.PlayerChampionshipResults[1].Lost, result2.PlayerChampionshipResults[1].Lost);
            Assert.AreEqual(championsListDto.PlayerChampionshipResults[0].Won, result2.PlayerChampionshipResults[0].Won);
            Assert.AreEqual(championsListDto.PlayerChampionshipResults[1].Won, result2.PlayerChampionshipResults[1].Won);
        }

        [TestMethod]
        public void DtoErrorTestMethod()
        {
            string expectedXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><message type=\"error\">test</message>";
            DtoError errorDto = new DtoError("test");
            string result = testParser.Serialize(errorDto);
            result = Regex.Replace(result, @"\r\n", "");
            DtoError result2 = (DtoError)testParser.Deserialize(expectedXml);
            Assert.AreEqual<string>(expectedXml, result);
            Assert.AreEqual(errorDto.ErrorMessage, result2.ErrorMessage);
            Assert.AreEqual(errorDto.Type, result2.Type);
        }

        [TestMethod]
        public void DtoGameMasterLoginTestMethod()
        {
            string expectedXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><message type=\"gameMasterLogin\">  <gameMasterLogin id=\"test\" gameType=\"test\" playersMin=\"2\" playersMax=\"4\" /></message>";
            DtoGameMasterLogin gameMasterLoginDto = new DtoGameMasterLogin("test", "test", 2, 4);
            string result = testParser.Serialize(gameMasterLoginDto);
            DtoGameMasterLogin result2 = (DtoGameMasterLogin)testParser.Deserialize(expectedXml);
            result = Regex.Replace(result, @"\r\n", "");
            Assert.AreEqual<string>(expectedXml, result);
            Assert.AreEqual(gameMasterLoginDto.GameType, result2.GameType);
            Assert.AreEqual(gameMasterLoginDto.Id, result2.Id);
            Assert.AreEqual(gameMasterLoginDto.Type, result2.Type);
            Assert.AreEqual(gameMasterLoginDto.PlayersMin, result2.PlayersMin);
            Assert.AreEqual(gameMasterLoginDto.PlayersMax, result2.PlayersMax);
        }

        [TestMethod]
        public void DtoGameStateTestMethod()
        {
            //<gameState>  <tac x=\"5\" y=\"5\" />  </gameState>
            string expectedXml1 = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><message type=\"gameState\">  <gameId id=\"test\" />  <nextPlayer nick=\"test\" /></message>";
            string expectedXml2 = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><message type=\"gameState\">  <gameId id=\"test\" />  <gameOver>    <player nick=\"test1\" result=\"loser\" />    <player nick=\"test2\" result=\"winner\" />  </gameOver></message>";
            DtoGameState gameStateDto1 = new DtoGameState("test", "test");
            DtoGameState gameStateDto2 = new DtoGameState("test", new List<PlayerResult> { new PlayerResult("test1", "loser"), new PlayerResult("test2", "winner") });
            string result1 = testParser.Serialize(gameStateDto1);
            string result2 = testParser.Serialize(gameStateDto2);
            DtoGameState dtoResult1 = (DtoGameState)testParser.Deserialize(expectedXml1);
            DtoGameState dtoResult2 = (DtoGameState)testParser.Deserialize(expectedXml2);
            result1 = Regex.Replace(result1, @"\r\n", "");
            result2 = Regex.Replace(result2, @"\r\n", "");
            Assert.AreEqual<string>(expectedXml1, result1);
            Assert.AreEqual<string>(expectedXml2, result2);
            Assert.AreEqual(gameStateDto1.GameId, dtoResult1.GameId);
            Assert.AreEqual(gameStateDto1.Type, dtoResult1.Type);
            Assert.AreEqual(gameStateDto1.NextPlayerNick, dtoResult1.NextPlayerNick);
            Assert.AreEqual(gameStateDto2.GameId, dtoResult2.GameId);
            Assert.AreEqual(gameStateDto2.Type, dtoResult2.Type);
            Assert.AreEqual(gameStateDto2.ResultsOfPlayers[0].Nick, dtoResult2.ResultsOfPlayers[0].Nick);
            Assert.AreEqual(gameStateDto2.ResultsOfPlayers[1].Nick, dtoResult2.ResultsOfPlayers[1].Nick);
            Assert.AreEqual(gameStateDto2.ResultsOfPlayers[0].Result, dtoResult2.ResultsOfPlayers[0].Result);
            Assert.AreEqual(gameStateDto2.ResultsOfPlayers[1].Result, dtoResult2.ResultsOfPlayers[1].Result);
        }

        [TestMethod]
        public void DtoGameStateTicTacToeTestMethod()
        {
            string expectedXml1 = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><message type=\"gameState\">  <gameState>    <tac x=\"5\" y=\"5\" />  </gameState>  <gameId id=\"test\" />  <nextPlayer nick=\"test\" /></message>";
            string expectedXml2 = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><message type=\"gameState\">  <gameId id=\"test\" />  <gameOver>    <player nick=\"test1\" result=\"loser\" />    <player nick=\"test2\" result=\"winner\" />  </gameOver></message>";
            Tac tac = new Tac(5, 5);
            DtoGameStateTicTacToe gameStateDto0 = new DtoGameStateTicTacToe("test", "test");
            DtoGameStateTicTacToe gameStateDto1 = new DtoGameStateTicTacToe("test", "test", tac);
            DtoGameStateTicTacToe gameStateDto2 = new DtoGameStateTicTacToe("test", new List<PlayerResult> { new PlayerResult("test1", "loser"), new PlayerResult("test2", "winner") });
            string result0 = testParser.Serialize(gameStateDto0);
            string result1 = testParser.Serialize(gameStateDto1);
            string result2 = testParser.Serialize(gameStateDto2);
            DtoGameStateTicTacToe dtoResult1 = (DtoGameStateTicTacToe)testParser.Deserialize<DtoGameStateTicTacToe>(expectedXml1);
            DtoGameStateTicTacToe dtoResult2 = (DtoGameStateTicTacToe)testParser.Deserialize<DtoGameStateTicTacToe>(expectedXml2);
            result1 = Regex.Replace(result1, @"\r\n", "");
            result2 = Regex.Replace(result2, @"\r\n", "");
            Assert.AreEqual<string>(expectedXml1, result1);
            Assert.AreEqual<string>(expectedXml2, result2);
            Assert.AreEqual(gameStateDto1.GameId, dtoResult1.GameId);
            Assert.AreEqual(gameStateDto1.Type, dtoResult1.Type);
            Assert.AreEqual(gameStateDto1.NextPlayerNick, dtoResult1.NextPlayerNick);
            Assert.AreEqual(gameStateDto2.GameId, dtoResult2.GameId);
            Assert.AreEqual(gameStateDto2.Type, dtoResult2.Type);
            Assert.AreEqual(gameStateDto2.ResultsOfPlayers[0].Nick, dtoResult2.ResultsOfPlayers[0].Nick);
            Assert.AreEqual(gameStateDto2.ResultsOfPlayers[1].Nick, dtoResult2.ResultsOfPlayers[1].Nick);
            Assert.AreEqual(gameStateDto2.ResultsOfPlayers[0].Result, dtoResult2.ResultsOfPlayers[0].Result);
            Assert.AreEqual(gameStateDto2.ResultsOfPlayers[1].Result, dtoResult2.ResultsOfPlayers[1].Result);
        }

        [TestMethod]
        public void DtoLeaveGameTestMethod()
        {
            string expectedXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><message type=\"leaveGame\">  <gameId id=\"test\" /></message>";
            DtoLeaveGame leaveGameDto = new DtoLeaveGame("test");
            string result = testParser.Serialize(leaveGameDto);
            DtoLeaveGame result2 = (DtoLeaveGame)testParser.Deserialize(expectedXml);
            result = Regex.Replace(result, @"\r\n", "");
            Assert.AreEqual<string>(expectedXml, result);
            Assert.AreEqual(leaveGameDto.GameId, result2.GameId);
            Assert.AreEqual(leaveGameDto.Type, result2.Type);
        }

        [TestMethod]
        public void DtoLoginResponseTestMethod()
        {
            string expectedXml1 = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><message type=\"loginResponse\">  <response accept=\"no\" />  <error id=\"1\" /></message>";
            string expectedXml2 = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><message type=\"loginResponse\">  <response accept=\"yes\" /></message>";
            DtoLoginResponse loginResponseDto1 = new DtoLoginResponse("no", 1);
            DtoLoginResponse loginResponseDto2 = new DtoLoginResponse("yes");
            string result1 = testParser.Serialize(loginResponseDto1);
            string result2 = testParser.Serialize(loginResponseDto2);
            DtoLoginResponse dtoResult1 = (DtoLoginResponse)testParser.Deserialize(expectedXml1);
            DtoLoginResponse dtoResult2 = (DtoLoginResponse)testParser.Deserialize(expectedXml2);
            result1 = Regex.Replace(result1, @"\r\n", "");
            result2 = Regex.Replace(result2, @"\r\n", "");
            Assert.AreEqual<string>(expectedXml1, result1);
            Assert.AreEqual<string>(expectedXml2, result2);
            Assert.AreEqual(loginResponseDto1.Accept, dtoResult1.Accept);
            Assert.AreEqual(loginResponseDto1.ErrorId, dtoResult1.ErrorId);
            Assert.AreEqual(loginResponseDto1.Type, dtoResult1.Type);
            Assert.AreEqual(loginResponseDto2.Accept, dtoResult2.Accept);
            Assert.AreEqual(loginResponseDto2.ErrorId, dtoResult2.ErrorId);
            Assert.AreEqual(loginResponseDto2.Type, dtoResult2.Type);
        }

        [TestMethod]
        public void DtoLogoutTestMethod()
        {
            string expectedXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><message type=\"logout\" />";
            DtoLogout logoutDto = new DtoLogout();
            string result = testParser.Serialize(logoutDto);
            result = Regex.Replace(result, @"\r\n", "");
            DtoLogout dtoResult = (DtoLogout)testParser.Deserialize(expectedXml);
            Assert.AreEqual<string>(expectedXml, result);
            Assert.AreEqual(logoutDto.Type, dtoResult.Type);
        }

        [TestMethod]
        public void DtoMessageTestMethod()
        {
            string expectedXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><message type=\"test\" />";
            DtoMessage messageDto = new DtoMessage("test");
            string result = testParser.Serialize(messageDto);
            result = Regex.Replace(result, @"\r\n", "");
            DtoMessage dtoResult = (DtoMessage)testParser.Deserialize(expectedXml);
            Assert.AreEqual<string>(expectedXml, result);
            Assert.AreEqual(messageDto.Type, dtoResult.Type);
        }

        [TestMethod]
        public void DtoMoveTestMethod()
        {
            string expectedXml1 = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><message type=\"move\">  <gameId id=\"test\"/>  <move>    <tic x=\"5\" y=\"5\" />  </move></message>";
            string expectedXml2 = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><message type=\"move\">  <gameId id=\"test\" /></message>";
            DtoMove expectedMoveDto = new DtoMove("test");
            DtoMove dtoResult = (DtoMove)testParser.Deserialize(expectedXml1);
            Assert.AreEqual(expectedMoveDto.GameId, dtoResult.GameId);
            Assert.AreEqual(expectedMoveDto.Type, dtoResult.Type);
            string result = testParser.Serialize(expectedMoveDto);
            result = Regex.Replace(result, @"\r\n", "");
            Assert.AreEqual<string>(expectedXml2, result);
        }

        [TestMethod]
        public void DtoMoveTicTacToeTestMethod()
        {
            string expectedXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><message type=\"move\">  <move>    <tic x=\"5\" y=\"5\" />  </move>  <gameId id=\"test\" /></message>";
            DtoMoveTicTacToe expectedDto = new DtoMoveTicTacToe("test", new Tic(5, 5));
            string result = testParser.Serialize(expectedDto);
            DtoMoveTicTacToe dtoResult = (DtoMoveTicTacToe)testParser.Deserialize<DtoMoveTicTacToe>(expectedXml);
            result = Regex.Replace(result, @"\r\n", "");
            Assert.AreEqual<string>(expectedXml, result);
            Assert.AreEqual(expectedDto.GameId, dtoResult.GameId);
            Assert.AreEqual(expectedDto.Type, dtoResult.Type);
            Assert.AreEqual(expectedDto.Move.X, dtoResult.Move.X);
            Assert.AreEqual(expectedDto.Move.Y, dtoResult.Move.Y);
        }

        [TestMethod]
        public void DtoPlayerLeftGameTestMethod()
        {
            string expectedXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><message type=\"playerLeftGame\">  <player nick=\"test\" />  <gameId id=\"test\" /></message>";
            DtoPlayerLeftGame expectedDto = new DtoPlayerLeftGame("test", "test");
            string result = testParser.Serialize(expectedDto);
            DtoPlayerLeftGame dtoResult = (DtoPlayerLeftGame)testParser.Deserialize(expectedXml);
            result = Regex.Replace(result, @"\r\n", "");
            Assert.AreEqual<string>(expectedXml, result);
            Assert.AreEqual(expectedDto.GameId, dtoResult.GameId);
            Assert.AreEqual(expectedDto.Nick, dtoResult.Nick);
            Assert.AreEqual(expectedDto.Type, dtoResult.Type);
        }

        [TestMethod]
        public void DtoPlayerLoginTestMethod()
        {
            string expectedXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><message type=\"playerLogin\">  <playerLogin nick=\"test\" gameType=\"test\" /></message>";
            DtoPlayerLogin expectedDto = new DtoPlayerLogin("test", "test");
            string result = testParser.Serialize(expectedDto);
            DtoPlayerLogin dtoResult = (DtoPlayerLogin)testParser.Deserialize(expectedXml);
            result = Regex.Replace(result, @"\r\n", "");
            Assert.AreEqual<string>(expectedXml, result);
            Assert.AreEqual(expectedDto.GameType, dtoResult.GameType);
            Assert.AreEqual(expectedDto.Nick, dtoResult.Nick);
            Assert.AreEqual(expectedDto.Type, dtoResult.Type);
        }

        [TestMethod]
        public void DtoServerShutdownTestMethod()
        {
            string expectedXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><message type=\"serverShutdown\" />";
            DtoServerShutdown expectedDto = new DtoServerShutdown();
            string result = testParser.Serialize(expectedDto);
            result = Regex.Replace(result, @"\r\n", "");
            DtoServerShutdown dtoResult = (DtoServerShutdown)testParser.Deserialize(expectedXml);
            Assert.AreEqual<string>(expectedXml, result);
            Assert.AreEqual(expectedDto.Type, dtoResult.Type);
        }

        [TestMethod]
        public void DtoThankyouTestMethod()
        {
            string expectedXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><message type=\"thank you\">  <gameId id=\"test\" /></message>";
            DtoThankyou expectedDto = new DtoThankyou("test");
            string result = testParser.Serialize(expectedDto);
            DtoThankyou result2 = (DtoThankyou)testParser.Deserialize(expectedXml);
            result = Regex.Replace(result, @"\r\n", "");
            Assert.AreEqual<string>(expectedXml, result);
            Assert.AreEqual(expectedDto.GameId, result2.GameId);
            Assert.AreEqual(expectedDto.Type, result2.Type);
        }
    }
}
