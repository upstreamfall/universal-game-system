﻿using System;
using Server;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests.Server
{
    [TestClass]
    public class GameMasterManagerTests
    {
        [TestMethod]
        public void AddGameMasterTest()
        {
            GameMastersManager gmm = new GameMastersManager();
            XmlParser.Dtos.DtoGameMasterLogin gml = new XmlParser.Dtos.DtoGameMasterLogin("test", "test", 2, 2);
            gmm.AddGameMaster(gml, new IOSockets.Commons.Connection());
            Assert.IsTrue(gmm.IsGMRegistered("test"));
        }

        [TestMethod]
        public void RemoveGameMasterTest()
        {
            GameMastersManager gmm = new GameMastersManager();
            XmlParser.Dtos.DtoGameMasterLogin gml = new XmlParser.Dtos.DtoGameMasterLogin("test", "test", 2, 2);
            gmm.AddGameMaster(gml, new IOSockets.Commons.Connection());
            Assert.IsTrue(gmm.IsGMRegistered("test"));
            gmm.RemoveGameMaster("test");
            Assert.IsFalse(gmm.IsGMRegistered("test"));
        }
    }
}
