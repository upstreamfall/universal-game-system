﻿using System;
using Server.States;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests.Server
{
    [TestClass]
    public class StateMachineTests
    {
        [TestMethod]
        public void RegisterStateTest()
        {
            StateMachine sm = new StateMachine(null, "idleState");
            sm.RegisterState("test1", null);
            Assert.IsTrue(sm.IsRegistered("test1"));
        }

        [TestMethod]
        public void ChangeStateTest()
        {
            StateMachine sm = new StateMachine(null, "idleState");
            sm.RegisterState("test1", new IdleState(sm));
            sm.RegisterState("test2", new IdleState(sm));
            Assert.IsTrue(sm.IsRegistered("test1"));
            Assert.IsTrue(sm.IsRegistered("test2"));
            sm.ChangeState("test1", null, null);
            Assert.IsTrue(sm.State.Name == "test1");
            sm.ChangeState("test2", null, null);
            Assert.IsTrue(sm.State.Name == "test2");
            Assert.IsTrue(sm.IsRegistered("test1"));
        }
    }
}
