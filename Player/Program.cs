﻿using log4net;
using System;
using System.Threading;

namespace Player
{
    class Program
    {
        protected static readonly ILog logger = LogManager.GetLogger(typeof(Program));

        static void Usage()
        {
            logger.Warn("[Wrong usage] Player closing in 5 seconds. Please try: ./Player --connect_to IP:PORT --nick Nick");
            Thread.Sleep(5000);
            logger.Info("Player closing");
            Environment.Exit(0);
        }

        static void Main(string[] args)
        {
            //check for proper startup params
            if (args.Length != 4)
            {
                Usage();
                return;
            }
            if (!args[0].Equals("--connect_to") && !args[2].Equals("--nick") && !args[1].Contains(":"))
            {
                Usage();
                return;
            }
            string[] split = args[1].Split(':');
            string ip = split[0];
            string nick = args[3];
            short port;
            if (!short.TryParse(split[1], out port))
            {
                Usage();
                return;
            }

            string gameType = "5-in-line-tic-tac-toe";
            string gameId = "0";
            string error = "test error";
            
            //Player initialization
            Player player = new Player(nick, gameType, ip, port);

            //for tests
            //while (true)
            //{
            //    Console.WriteLine("1 - send playerLogin");
            //    Console.WriteLine("2 - send move");
            //    Console.WriteLine("3 - send thank you");
            //    Console.WriteLine("4 - send leave game");
            //    Console.WriteLine("5 - send logout");
            //    Console.WriteLine("6 - send error");

            //    string input = Console.ReadLine();

            //    switch (input[0])
            //    {
            //        case '1':
            //            player.GetCommunicator().SendLogin(nick, gameType);
            //            break;
            //        case '2':
            //            player.GetCommunicator().SendMove(gameId, player.GetAI().SelectMove(gameId));
            //            break;
            //        case '3':
            //            player.GetCommunicator().SendThankYou(gameId);
            //            break;
            //        case '4':
            //            player.GetCommunicator().SendLeaveGame(gameId);
            //            break;
            //        case '5':
            //            player.GetCommunicator().SendLogout();
            //            break;
            //        case '6':
            //            player.GetCommunicator().SendError(error);
            //            break;
            //        default:
            //            logger.Info("Wrong option chosen");
            //            break;
            //    }
            //}
        }
    }
}