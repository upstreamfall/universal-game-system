﻿using IOSockets.ClientSockets;
using IOSockets.Commons;
using log4net;
using System;
using System.Threading;
using XmlParser;
using XmlParser.Dtos;

namespace Player
{
    /// <summary>
    /// Class responsible for communication between player and server (sends, receives, serializes and deserializes messages)
    /// </summary>
    class Communicator
    {
        protected static readonly ILog logger = LogManager.GetLogger(typeof(Communicator));
        static Player player;
        ClientSocketCallbacks csc;
        ClientSocket cs;
        static Parser par;

        /// <summary>
        /// Constructor for Communicator
        /// </summary>
        /// <param name="_player">Player whose communicator it is</param>
        /// <param name="_ip">Server IP address</param>
        /// <param name="_port">Server port number</param>
        public Communicator(Player _player, string _ip, short _port)
        {
            player = _player;
            csc = new ClientSocketCallbacks(MessageReceivedHandler, ClosedConnectionHandler);
            cs = new ClientSocket(_ip, _port, csc);
            par = new Parser();
        }

        void MessageReceivedHandler(Message m)
        {
            DtoMessage received = par.Deserialize(m.message);
            switch (received.Type)
            {
                case "loginResponse":
                    LoginResponseReceived(m);
                    break;
                case "gameState":
                    GameStateReceived(m);
                    break;
                case "serverShutdown":
                    ServerShutdownReceived(m);
                    break;
                case "championsList":
                    ChampionsListReceived(m);
                    break;
                case "error":
                    ErrorReceived(m);
                    break;
                default:
                    UnknownReceived(m);
                    break;
            }
        }

        void ClosedConnectionHandler(Connection c)
        {
            logger.Warn("[Connection closed] Player closing in 5 seconds");
            Console.WriteLine("[Connection closed] Player closing in 5 seconds");
            Thread.Sleep(5000);
            logger.Info("Player closing");
            Console.WriteLine("Player closing");
            Environment.Exit(0);
        }

        void LoginResponseReceived(Message m)
        {
            DtoLoginResponse received = par.Deserialize<DtoLoginResponse>(m.message);
            player.LoginResponseReceived(received);
        }

        void GameStateReceived(Message m)
        {
            DtoGameStateTicTacToe received = par.Deserialize<DtoGameStateTicTacToe>(m.message);
            player.GameStateReceived(received);
        }

        void ServerShutdownReceived(Message m)
        {
            DtoServerShutdown received = par.Deserialize<DtoServerShutdown>(m.message);
            player.ServerShutdownReceived(received);
        }

        void ChampionsListReceived(Message m)
        {
            DtoChampionsList received = par.Deserialize<DtoChampionsList>(m.message);
            player.ChampionsListReceived(received);
        }

        void ErrorReceived(Message m)
        {
            DtoError received = par.Deserialize<DtoError>(m.message);
            player.ErrorReceived(received);
        }

        void UnknownReceived(Message m)
        {
            logger.Warn("[Unsupported message received] " + m.message);
            Console.WriteLine("[Unsupported message received] " + m.message);
        }
        /// <summary>
        /// Sends PlayerLogin message
        /// </summary>
        /// <param name="nick">Player nick</param>
        /// <param name="gameType">Player game type</param>
        public void SendLogin(string nick, string gameType)
        {
            cs.connection.SendMsg(par.Serialize(new DtoPlayerLogin(nick, gameType)));
            logger.Info("[PlayerLogin sent] Nick: " + nick + " game type: " + gameType);
            Console.WriteLine("[PlayerLogin sent] Nick: " + nick + " game type: " + gameType);
        }
        /// <summary>
        /// Sends MoveTicTacToe message
        /// </summary>
        /// <param name="gameId">Affected game Id</param>
        /// <param name="move">Move as [x,y] table</param>
        public void SendMove(string gameId, int[] move)
        {
            cs.connection.SendMsg(par.Serialize(new DtoMoveTicTacToe(gameId, new Tic(move[0], move[1]))));
            logger.Info("[Move sent] GameId: " + gameId + " X:" + move[0].ToString() + " Y:" + move[1].ToString());
            Console.WriteLine("[Move sent] GameId: " + gameId + " X:" + move[0].ToString() + " Y:" + move[1].ToString());
        }
        /// <summary>
        /// Sends Thankyou message
        /// </summary>
        /// <param name="gameId">Affected game id</param>
        public void SendThankYou(string gameId)
        {
            cs.connection.SendMsg(par.Serialize(new DtoThankyou(gameId)));
            logger.Info("[ThankYou sent] GameId: " + gameId);
            Console.WriteLine("[ThankYou sent] GameId: " + gameId);
        }
        /// <summary>
        /// Sends LeaveGame message
        /// </summary>
        /// <param name="gameId">Affected game id</param>
        public void SendLeaveGame(string gameId)
        {
            cs.connection.SendMsg(par.Serialize(new DtoLeaveGame(gameId)));
            logger.Info("[LeaveGame sent] GameId: " + gameId);
            Console.WriteLine("[LeaveGame sent] GameId: " + gameId);
        }
        /// <summary>
        /// Sends Logout message
        /// </summary>
        public void SendLogout()
        {
            cs.connection.SendMsg(par.Serialize(new DtoLogout()));
            logger.Info("[Logout sent]");
            Console.WriteLine("[Logout sent]");
        }
        /// <summary>
        /// Sends Error message
        /// </summary>
        /// <param name="errorMessage">Message</param>
        public void SendError(string errorMessage)
        {
            cs.connection.SendMsg(par.Serialize(new DtoError(errorMessage)));
            logger.Info("[Error sent] " + errorMessage);
            Console.WriteLine("[Error sent] " + errorMessage);
        }
    }
}
