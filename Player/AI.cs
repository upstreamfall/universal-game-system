﻿using System;

namespace Player
{
    /// <summary>
    /// Class responsible for game AI
    /// </summary>
    class AI
    {
        /// <summary>
        /// Updates local board state with last move
        /// </summary>
        /// <param name="gameId">Affected game id</param>
        /// <param name="player">player whose move is it</param>
        /// <param name="x">coordinate x</param>
        /// <param name="y">coordinate y</param>
        public void UpdateBoard(string gameId, short player, int? x, int? y)
        {
            //TODO: add functionality
        }

        /// <summary>
        /// Selects next move according to board state. For now, simple random move.
        /// </summary>
        /// <param name="gameId">Affected game id</param>
        /// <returns>Selected move</returns>
        public int[] SelectMove(string gameId)
        {
            int[] move = new int[2];
            Random rnd = new Random();
            move[0] = rnd.Next(5);
            move[1] = 0;
            return move;
        }
    }
}
