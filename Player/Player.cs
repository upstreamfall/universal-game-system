﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using XmlParser.Dtos;

namespace Player
{
    /// <summary>
    /// Class responsible for Player logic (gives AI last move, asks AI for next move, sends ending messages when needed)
    /// </summary>
    class Player
    {
        protected static readonly ILog logger = LogManager.GetLogger(typeof(Player));
        string nick;
        string gameType;
        Communicator communicator;
        AI ai;

        /// <summary>
        /// Constructor for Player
        /// </summary>
        /// <param name="_nick">Player nick</param>
        /// <param name="_gameType">Player game type</param>
        /// <param name="_ip">Server IP address</param>
        /// <param name="_port">Server port number</param>
        public Player(string _nick, string _gameType, string _ip, short _port)
        {
            nick = _nick;
            gameType = _gameType;
            try
            {
                communicator = new Communicator(this, _ip, _port);
            }
            catch (Exception e)
            {
                logger.Error("Connection with the server cannot be established");
                Console.WriteLine("Connection with the server cannot be established");
                logger.Info("Player closing in 5 seconds");
                Console.WriteLine("Player closing in 5 seconds");
                Thread.Sleep(5000);
                logger.Info("Player closing");
                Console.WriteLine("Player closing");
                Environment.Exit(0);
            }
            ai = new AI();
            logger.Info("Player " + nick + " started, connected with " + _ip + " on port " + _port.ToString() + " playing " + gameType);
            Console.WriteLine("Player " + nick + " started, connected with " + _ip + " on port " + _port.ToString() + " playing " + gameType);
            //line below commented for tests - login is send manually
            communicator.SendLogin(nick, gameType);
        }
        /// <summary>
        /// Analyzes received LoginResponse message
        /// </summary>
        /// <param name="received">Received message</param>
        public void LoginResponseReceived(DtoLoginResponse received)
        {
            //login accepted
            if (received.Accept.Equals("yes"))
            {
                logger.Info("[Login response received] Positive");
                Console.WriteLine("[Login response received] Positive");
            }
            //login denied
            else if (received.Accept.Equals("no"))
            {
                //check for error
                switch (received.ErrorId)
                {
                    case 1:
                        logger.Info("[Login response received] Negative: wrong nick");
                        Console.WriteLine("[Login response received] Negative: wrong nick");
                        break;
                    case 2:
                        logger.Info("[Login response received] Negative: improper game type");
                        Console.WriteLine("[Login response received] Negative: improper game type");
                        break;
                    case 3:
                        logger.Info("[Login response received] Negative: players pool overflow");
                        Console.WriteLine("[Login response received] Negative: players pool overflow");
                        break;
                    default:
                        logger.Warn("[Login response received] Negative: unknown error");
                        Console.WriteLine("[Login response received] Negative: unknown error");
                        break;
                }
                //close player
                logger.Info("Player closing in 5 seconds");
                Console.WriteLine("Player closing in 5 seconds");
                Thread.Sleep(5000);
                logger.Info("Player closing");
                Console.WriteLine("Player closing");
                Environment.Exit(0);
            }
        }
        /// <summary>
        /// Analyzes received GameState message
        /// </summary>
        /// <param name="received">Received message</param>
        public void GameStateReceived(DtoGameStateTicTacToe received)
        {
            //received results => game is over, show results
            if (received.ResultsOfPlayers != null)
            {
                int nr = 0;
                string result = "[Game state received] Game ended, received results\n";
                foreach (PlayerResult PR in received.ResultsOfPlayers)
                {
                   result += "\n" + ++nr + ". Player: " + PR.Nick + ", Result: " + PR.Result;
                }
                logger.Info(result);
                Console.WriteLine(result);
                communicator.SendThankYou(received.GameId);
            }
            //no results received => game is on
            else
            {
                //received move
                if (received.Move != null)
                {
                    logger.Info("[Game state received] Next player: " + received.NextPlayerNick + ", received move: X:" + received.Move.X + " Y:" + received.Move.Y);
                    Console.WriteLine("[Game state received] Next player: " + received.NextPlayerNick + ", received move: X:" + received.Move.X + " Y:" + received.Move.Y);
                    //it's our turn => update board state and send our move
                    if (received.NextPlayerNick.Equals(nick))
                    {
                        ai.UpdateBoard(received.GameId, 1, received.Move.X, received.Move.Y);
                        communicator.SendMove(received.GameId, ai.SelectMove(received.GameId));
                    }
                    //it's someone else turn => only update board state
                    else
                    {
                        ai.UpdateBoard(received.GameId, 2, received.Move.X, received.Move.Y);
                    }
                }
                //no move received => the game has just started so there is no need to update board state
                else
                {
                    logger.Info("[Game state received] Next player: " + received.NextPlayerNick + ", received no move");
                    Console.WriteLine("[Game state received] Next player: " + received.NextPlayerNick + ", received no move");
                    //it's our turn => send our move
                    if (received.NextPlayerNick.Equals(nick))
                    {
                        communicator.SendMove(received.GameId, ai.SelectMove(received.GameId));
                    }
                }
            }
        }
        /// <summary>
        /// Analyzes received ServerShutdown message
        /// </summary>
        /// <param name="received">Received message</param>
        public void ServerShutdownReceived(DtoServerShutdown received)
        {
            //Close program
            logger.Info("[ServerShutdown received] Player closing in 5 seconds");
            Console.WriteLine("[ServerShutdown received] Player closing in 5 seconds");
            Thread.Sleep(5000);
            logger.Info("Player closing");
            Console.WriteLine("Player closing");
            Environment.Exit(0);
        }
        /// <summary>
        /// Analyzes received ChampionsList message
        /// </summary>
        /// <param name="received">Received message</param>
        public void ChampionsListReceived(DtoChampionsList received)
        {
            logger.Info("[ChampionsList received]");
            Console.WriteLine("[ChampionsList received]");
        }
        /// <summary>
        /// Analyzes received error message
        /// </summary>
        /// <param name="received">Received message</param>
        public void ErrorReceived(DtoError received)
        {
            logger.Info("[Error received] " + received.ErrorMessage);
            Console.WriteLine("[Error received] " + received.ErrorMessage);
        }
        /// <summary>
        /// Returns communicator for sending messages
        /// </summary>
        /// <returns>Communicator for sending messages</returns>
        public Communicator GetCommunicator()
        {
            return communicator;
        }
        /// <summary>
        /// Returns ai for selecting moves
        /// </summary>
        /// <returns>AI for selecting moves</returns>
        public AI GetAI()
        {
            return ai;
        }
    }
}
