﻿using IOSockets.Commons;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using XmlParser.Dtos;

namespace Server
{
    /// <summary>
    /// Container for game master properties
    /// </summary>
    public class GameMaster
    {
        public string Id;
        public string GameType;
        public int PlayersMin;
        public int PlayersMax;
        public Connection Connection;
    }

    /// <summary>
    /// Class responslibles for manage, send to/receive for messages game masters 
    /// </summary>
    public class GameMastersManager : IGameMastersManager
    {
        protected static readonly ILog logger = LogManager.GetLogger(typeof(GameMastersManager));

        /// <summary>
        /// key is the game master nick
        /// </summary>
        private Dictionary<string, GameMaster> _gameMasters;
        private Dictionary<Connection, string> _gmConnections;
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Create new container for game masters
        /// </summary>
        public GameMastersManager()
        {
            _gameMasters = new Dictionary<string, GameMaster>();
            _gmConnections = new Dictionary<Connection, string>();
        }

        public void AddGameMaster(DtoGameMasterLogin gameMasterLoginDto, Connection connection)
        {
            GameMaster newGameMaster = CreateGameMasterFromDto(gameMasterLoginDto, connection);
            _gameMasters.Add(newGameMaster.Id, newGameMaster);
            _gmConnections.Add(connection, gameMasterLoginDto.Id);
        }

        public bool IsGMRegistered(Connection connection)
        {
            if (_gmConnections.ContainsKey(connection)) 
                return true;
            else return false;
        }

        public bool IsGMRegistered(GameMaster gm)
        {
            if (_gameMasters.ContainsValue(gm)) return true;
            else return false;
        }

        public bool IsGMRegistered(string id)
        {
            if (_gameMasters.ContainsKey(id)) return true;
            else return false;
        }

        public void RemoveGameMaster(string id)
        {
            _gmConnections.Remove(_gameMasters[id].Connection);
            _gameMasters.Remove(id);
        }

        public void SendMessageToAllGameMasters(string message)
        {
            foreach (GameMaster gm in _gameMasters.Values)
                gm.Connection.SendMsg(message);
        }

        public void SendMessageToGM(string message, string id)
        {
            _gameMasters[id].Connection.SendMsg(message);
        }

        public GameMaster CreateGameMasterFromDto(DtoGameMasterLogin gameMasterLoginDto)
        {
            GameMaster newGameMaster = new GameMaster();
            newGameMaster.Id = gameMasterLoginDto.Id;
            newGameMaster.GameType = gameMasterLoginDto.GameType;
            newGameMaster.PlayersMin = gameMasterLoginDto.PlayersMin;
            newGameMaster.PlayersMax = gameMasterLoginDto.PlayersMax;
            return newGameMaster;
        }

        public GameMaster CreateGameMasterFromDto(DtoGameMasterLogin gameMasterLoginDto, Connection connection)
        {
            GameMaster newGameMaster = new GameMaster();
            newGameMaster.Id = gameMasterLoginDto.Id;
            newGameMaster.GameType = gameMasterLoginDto.GameType;
            newGameMaster.PlayersMin = gameMasterLoginDto.PlayersMin;
            newGameMaster.PlayersMax = gameMasterLoginDto.PlayersMax;
            newGameMaster.Connection = connection;
            return newGameMaster;
        }

        public bool FindGameMasterWithGameType(string gameType, out GameMaster gameMaster)
        {
            try
            {
                gameMaster = _gameMasters.First(t => t.Value.GameType == gameType).Value;
                return true;
            }
            catch (InvalidOperationException)
            {
                log.Debug("Can't find GM with gameType = " + gameType);
                gameMaster = null;
                return false;
            }
        }

        public bool GetConnectionFromGM(string gameMasterId, out Connection connection)
        {
            try
            {
                connection = _gameMasters.First(t => t.Key == gameMasterId).Value.Connection;
                return true;
            }
            catch (InvalidOperationException)
            {
                log.Debug("Can't find GM with gameMasterId = " + gameMasterId);
                connection = null;
                return false;
            }    
        }

        public string GetGMIdByConnection(Connection connection)
        {
            if (_gmConnections.ContainsKey(connection))
                return _gmConnections[connection];
            else return string.Empty;
        }
    }
}
