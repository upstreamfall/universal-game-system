﻿using System;
using XmlParser.Dtos;

namespace Server.States
{
    /// <summary>
    /// State to handle connection closing from player
    /// </summary>
    public class ConnectionWithPlayerClosedState : StateBase
    {
        /// <summary>
        /// Create new state
        /// </summary>
        /// <param name="stateMachine">reference to the state machine</param>
        public ConnectionWithPlayerClosedState(StateMachine stateMachine) : base(stateMachine) { }

        /// <summary>
        /// Send information to game master and delete player from container
        /// </summary>
        /// <param name="owner">Server object to make changes in managers</param>
        /// <param name="previousStateName">Previous state of state machine</param>
        /// <param name="message">Contain connection to send next message</param>
        /// <param name="dtoMes">Deserialized message to handle in this state</param>
        public override void OnEnter(Server owner, string previousStateName, IOSockets.Commons.Message message, XmlParser.Dtos.DtoMessage dtoMes)
        {
            if (!(dtoMes is DtoLogout))
                throw new ArgumentException("Wrong Dto type in OnEnter event function");
            DtoPlayerLeftGame playerLeftAGame = null;
            if (this.CheckPlayerRegistration(owner, message))
            {
                string nick = owner.PlayersMan.GetPlayerNick(message.connection);
                string info = "Connection closed with player " + nick;
                playerLeftAGame = LogoutPlayer(owner, nick);
                info += " (player logged out)";
                log.Info(info);
            }
            if (playerLeftAGame != null)
                this._stateMachine.ChangeState("playerLeftGame", message, playerLeftAGame);
            else this._stateMachine.ChangeState(this._stateMachine.IdleStateName, message, dtoMes);
        }

        public override void OnExit(Server owner, string nextStateName)
        {
        }

        private DtoPlayerLeftGame LogoutPlayer(Server owner, string nick)
        {
            DtoPlayerLeftGame playerLeftAGame = null;
            Player p = owner.PlayersMan.GetPlayerByNick(nick);
            if (p.isPlaying)
            {
                owner.GamesMan.RemovePlayerFromGame(p);
                playerLeftAGame = new DtoPlayerLeftGame(p.GameId, p.Nick);
            }
            owner.PlayersMan.RemovePlayer(nick);
            return playerLeftAGame;
        }
    }
}
