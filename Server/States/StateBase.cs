﻿using IOSockets.Commons;
using XmlParser.Dtos;

namespace Server.States
{
    /// <summary>
    /// Abstract class for handling messages
    /// </summary>
    public abstract class StateBase
    {
        protected static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Create new state
        /// </summary>
        /// <param name="stateMachine">reference to the state machine</param>
        public StateBase(StateMachine stateMachine)
        {
            _stateMachine = stateMachine;
        }

        protected StateMachine _stateMachine;
        public string Name { get; set; }

        /// <summary>
        /// Make action when message with property type receives
        /// </summary>
        /// <param name="owner">Server object to make changes in managers</param>
        /// <param name="previousStateName">Previous state of state machine</param>
        /// <param name="message">Contain connection to send next message</param>
        /// <param name="dtoMes">Deserialized message to handle in this state</param>
        abstract public void OnEnter(Server owner, string previousStateName, Message message, DtoMessage dtoMes);

        /// <summary>
        /// Make action when state complited work
        /// </summary>
        /// <param name="owner">Server reference, which contains state machine</param>
        /// <param name="nextStateName">Next state after finish this</param>
        abstract public void OnExit(Server owner, string nextStateName);

        protected bool CheckPlayerRegistration(Server owner, Message message)
        {
            if (owner.PlayersMan.IsPlayerRegistered(message.connection))
                return true;
            else
            {
                log.Warn("Received message from client not registered as player!!");
                log.Debug("Unknown source's message:\n" + message.message);
                message.connection.SendMsg(owner.XmlParser.Serialize(new DtoError("You're not registered as player! Please log in first.")));
                return false;
            }
        }

        protected bool CheckGameMasterRegistration(Server owner, Message message)
        {
            if (owner.GameMastersMan.IsGMRegistered(message.connection))
                return true;
            else
            {
                log.Warn("Received message from client not registered as Game Master!!");
                log.Debug("Unknown source's message:\n" + message.message);
                message.connection.SendMsg(owner.XmlParser.Serialize(new DtoError("You're not registered as Game Master! Please log in first.")));
                return false;
            }
        }
    }
}
