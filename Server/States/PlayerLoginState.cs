﻿using IOSockets.Commons;
using System;
using XmlParser.Dtos;

namespace Server.States
{
    /// <summary>
    /// State to handle message with type 'playerLogin'
    /// </summary>
    public class PlayerLoginState : StateBase
    {
        /// <summary>
        /// Create new state
        /// </summary>
        /// <param name="stateMachine">reference to the state machine</param>
        public PlayerLoginState(StateMachine stateMachine) : base(stateMachine) { }

        /// <summary>
        /// Register new player and check if new game can start
        /// </summary>
        /// <param name="owner">Server object to make changes in managers</param>
        /// <param name="previousStateName">Previous state of state machine</param>
        /// <param name="message">Contain connection to send next message</param>
        /// <param name="dtoMes">Deserialized message to handle in this state</param>
        public override void OnEnter(Server owner, string previousStateName, Message message, DtoMessage dtoMes)
        {
            if (!(dtoMes is DtoPlayerLogin)) 
                throw new ArgumentException("Wrong Dto type in OnEnter event function");
            DtoPlayerLogin playerLogin = dtoMes as DtoPlayerLogin;
            string response;

            if (playerLogin.GameType.Equals("") || null == playerLogin.GameType)
            {
                log.Warn("Player game type is empty!");
                response = owner.XmlParser.Serialize(new DtoLoginResponse("no", (int)LoginResponseError.Improper_game_type));
                log.Debug("Sending LoginResponse:\n" + response + "\n");
                message.connection.SendMsg(response);
                this._stateMachine.ChangeState(this._stateMachine.IdleStateName, message, dtoMes);
            }
            else if (!owner.PlayersMan.IsPlayerRegistered(playerLogin.Nick))
            {
                if (owner.PlayersMan.AddPlayer(dtoMes as DtoPlayerLogin, message.connection))
                {
                    log.Info("Player: nick=\"" + playerLogin.Nick + "\" gameType=\"" + playerLogin.GameType + "\" logged in successfully");
                    response = owner.XmlParser.Serialize(new DtoLoginResponse("yes"));
                    log.Debug("Sending LoginResponse:\n" + response + "\n");
                    message.connection.SendMsg(response);
                    this._stateMachine.ChangeState("beginGame", message, dtoMes);
                }
                else
                {
                    log.Warn("Players pool is full!");
                    response = owner.XmlParser.Serialize(new DtoLoginResponse("no", (int)LoginResponseError.Player_pool_overflow));
                    log.Debug("Sending LoginResponse:\n" + response + "\n");
                    message.connection.SendMsg(response);
                }
            }
            else    // Player with this nick is already registered!
            {
                log.Warn("Player login failed - nick is already registered");
                response = owner.XmlParser.Serialize(new DtoLoginResponse("no",(int)LoginResponseError.Wrong_nick));
                log.Debug("Sending LoginResponse:\n"+response+"\n");
                message.connection.SendMsg(response);
                this._stateMachine.ChangeState(this._stateMachine.IdleStateName, message, dtoMes);
            }
        }

        public override void OnExit(Server owner, string nextStateName)
        {
        }
    }
}
