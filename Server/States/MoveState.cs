﻿using System;
using XmlParser.Dtos;

namespace Server.States
{
    /// <summary>
    /// State to handle message with type 'move'
    /// </summary>
    public class MoveState : StateBase
    {
        /// <summary>
        /// Create new instance of state
        /// </summary>
        /// <param name="stateMachine">reference to the state machine</param>
        public MoveState(StateMachine stateMachine) : base(stateMachine) { }

        /// <summary>
        /// Forward move to game master
        /// </summary>
        /// <param name="owner">Server object to make changes in managers</param>
        /// <param name="previousStateName">Previous state of state machine</param>
        /// <param name="message">Contain connection to send next message</param>
        /// <param name="dtoMes">Deserialized message to handle in this state</param>
        public override void OnEnter(Server owner, string previousStateName, IOSockets.Commons.Message message, XmlParser.Dtos.DtoMessage dtoMes)
        {
            if (!(dtoMes is DtoMove)) 
                throw new ArgumentException("Wrong Dto type in OnEnter event function");
            DtoMove move = (dtoMes as DtoMove);
            if (this.CheckPlayerRegistration(owner, message))
            {
                string nick = owner.PlayersMan.GetPlayerNick(message.connection);
                log.Info("Move received from player " + nick + " in game " + move.GameId);
                string gmId = owner.GamesMan.GetGameMasterIdFromGame(move.GameId);
                if (!gmId.Equals(string.Empty))
                {
                    log.Debug("Sending message move to GM with id=" + gmId.ToString() + " message:\n" + message.message);
                    owner.GameMastersMan.SendMessageToGM(message.message, gmId);
                }
                else
                {
                    log.Warn("There is no game with id = " + move.GameId + "sending error to player!");
                    string errorMessage = owner.XmlParser.Serialize(new DtoError("No game with id = " + move.GameId + " registered"));
                    owner.PlayersMan.SendMessageToPlayer(errorMessage, nick);
                }
            }
            this._stateMachine.ChangeState(this._stateMachine.IdleStateName, message, dtoMes);
        }

        public override void OnExit(Server owner, string nextStateName)
        {
        }
    }
}
