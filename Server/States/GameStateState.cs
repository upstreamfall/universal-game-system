﻿using System;
using System.Collections.Generic;
using XmlParser.Dtos;

namespace Server.States
{
    /// <summary>
    /// State to handle message with type 'gameState'
    /// </summary>
    public class GameStateState : StateBase
    {
        /// <summary>
        /// Create new instance
        /// </summary>
        /// <param name="stateMachine">reference to the state machine</param>
        public GameStateState(StateMachine stateMachine) : base(stateMachine) { }

        /// <summary>
        /// Handle gameState and send to all players in game
        /// </summary>
        /// <param name="owner">Server object to make changes in managers</param>
        /// <param name="previousStateName">Previous state of state machine</param>
        /// <param name="message">Contain connection to send next message</param>
        /// <param name="dtoMes">Deserialized message to handle in this state</param>
        public override void OnEnter(Server owner, string previousStateName, IOSockets.Commons.Message message, XmlParser.Dtos.DtoMessage dtoMes)
        {
            if (!(dtoMes is DtoGameState)) 
                throw new ArgumentException("Wrong Dto type in OnEnter event function");
            DtoGameState gs = (dtoMes as DtoGameState);
            if (this.CheckGameMasterRegistration(owner, message))
            {
                log.Info("Received gameState in game " + gs.GameId + " from GM with id = " + owner.GameMastersMan.GetGMIdByConnection(message.connection));
                List<Player> players = owner.GamesMan.GetPlayersFromGame(gs.GameId);
                if (players != null)
                    players.ForEach(x => { owner.PlayersMan.SendMessageToPlayer(message.message, x.Nick); });
                if (gs.ResultsOfPlayers != null)    // if gameOver then end game
                {
                    foreach (Player p in players)
                        owner.PlayersMan.PlayerLeaveGame(p.Nick);
                    owner.GamesMan.RemoveGame(gs.GameId);
                }
            }
            this._stateMachine.ChangeState(this._stateMachine.IdleStateName, message, dtoMes);
        }

        public override void OnExit(Server owner, string nextStateName)
        {
        }
    }
}
