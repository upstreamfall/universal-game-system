﻿using System;
using XmlParser.Dtos;

namespace Server.States
{
    /// <summary>
    /// State to handle message with type 'playerLeftGame' sending to game master
    /// </summary>
    public class PlayerLeftGameState : StateBase
    {
        /// <summary>
        /// Create new instance of state
        /// </summary>
        /// <param name="stateMachine">reference to the state machine</param>
        public PlayerLeftGameState(StateMachine stateMachine) : base(stateMachine) { }

        /// <summary>
        /// Send to game master information about left player
        /// </summary>
        /// <param name="owner">Server object to make changes in managers</param>
        /// <param name="previousStateName">Previous state of state machine</param>
        /// <param name="message">Contain connection to send next message</param>
        /// <param name="dtoMes">Deserialized message to handle in this state</param>
        public override void OnEnter(Server owner, string previousStateName, IOSockets.Commons.Message message, XmlParser.Dtos.DtoMessage dtoMes)
        {
            if (!(dtoMes is DtoPlayerLeftGame))
                throw new ArgumentException("Wrong Dto type in OnEnter event function");
            DtoPlayerLeftGame plg = dtoMes as DtoPlayerLeftGame;
            string mes = owner.XmlParser.Serialize(plg);
            GameMaster GM = owner.GamesMan.GetGameMasterFromGame(plg.GameId);
            GM.Connection.SendMsg(mes);
            if(previousStateName=="leaveGame")
                this._stateMachine.ChangeState("beginGame", null, new DtoPlayerLogin(plg.Nick, GM.GameType));
            else this._stateMachine.ChangeState(this._stateMachine.IdleStateName, message, dtoMes);
        }

        public override void OnExit(Server owner, string nextStateName)
        {
        }
    }
}
