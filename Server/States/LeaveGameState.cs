﻿using System;
using XmlParser.Dtos;

namespace Server.States
{
    /// <summary>
    /// Handle message with type 'leaveGame'
    /// </summary>
    public class LeaveGameState : StateBase
    {
        /// <summary>
        /// Create new instance
        /// </summary>
        /// <param name="stateMachine">reference to the state machine</param>
        public LeaveGameState(StateMachine stateMachine) : base(stateMachine) { }

        /// <summary>
        /// Delete player form game and send information to game master
        /// </summary>
        /// <param name="owner">Server object to make changes in managers</param>
        /// <param name="previousStateName">Previous state of state machine</param>
        /// <param name="message">Contain connection to send next message</param>
        /// <param name="dtoMes">Deserialized message to handle in this state</param>
        public override void OnEnter(Server owner, string previousStateName, IOSockets.Commons.Message message, XmlParser.Dtos.DtoMessage dtoMes)
        {
            if (!(dtoMes is DtoLeaveGame))
                throw new ArgumentException("Wrong Dto type in OnEnter event function");
            DtoLeaveGame leaveGame = dtoMes as DtoLeaveGame;
            DtoPlayerLeftGame playerLeftAGame = null;
            string info = string.Empty;
            if(this.CheckPlayerRegistration(owner, message))
            {
                string nick = owner.PlayersMan.GetPlayerNick(message.connection);
                info = "Server received \"leaveGame\" nr = "+leaveGame.GameId+" from "+nick;
                log.Info(info);
                playerLeftAGame = LeaveGame(owner, nick, leaveGame.GameId);
            }
            if (playerLeftAGame != null)
            {
                info = " (player left game)";
                log.Info(info);
                this._stateMachine.ChangeState("playerLeftGame", message, playerLeftAGame);
            }
            else this._stateMachine.ChangeState(this._stateMachine.IdleStateName, message, dtoMes);
        }

        public override void OnExit(Server owner, string nextStateName)
        {
        }

        private DtoPlayerLeftGame LeaveGame(Server owner, string nick, string gameId)
        {
            Player p = owner.PlayersMan.GetPlayerByNick(nick);
            if (!p.isPlaying || p.GameId != gameId)
            {
                log.Error("Player with this nick is not playing or is in different game! (Nick=" + nick + " gameId=" + gameId + ")");
                return null;
            }
            owner.GamesMan.RemovePlayerFromGame(p);
            owner.PlayersMan.PlayerLeaveGame(nick);
            return new DtoPlayerLeftGame(p.GameId, p.Nick);
        }
    }
}
