﻿using System;
using XmlParser.Dtos;

namespace Server.States
{
    /// <summary>
    /// State to handle message with type 'error'
    /// </summary>
    public class ErrorState : StateBase
    {
        /// <summary>
        /// Create new state
        /// </summary>
        /// <param name="stateMachine">reference to the state machine</param>
        public ErrorState(StateMachine stateMachine) : base(stateMachine) { }

        /// <summary>
        /// Send error message to client because of unhandled message type
        /// </summary>
        /// <param name="owner">Server object to make changes in managers</param>
        /// <param name="previousStateName">Previous state of state machine</param>
        /// <param name="message">Contain connection to send next message</param>
        /// <param name="dtoMes">Deserialized message to handle in this state</param>
        public override void OnEnter(Server owner, string previousStateName, IOSockets.Commons.Message message, XmlParser.Dtos.DtoMessage dtoMes)
        {
             if (!(dtoMes is DtoError))
                throw new ArgumentException("Wrong Dto type in OnEnter event function");
            DtoError error = dtoMes as DtoError;
            string name = "\"UNKNOWN\"";
            if(owner.PlayersMan.IsPlayerRegistered(message.connection))
                name = owner.PlayersMan.GetPlayerNick(message.connection);
            if(owner.GameMastersMan.IsGMRegistered(message.connection))
                name = owner.GameMastersMan.GetGMIdByConnection(message.connection);
            log.Warn("ERROR received from " + name + " error message:\n" + error.ErrorMessage);
            this._stateMachine.ChangeState(this._stateMachine.IdleStateName, message, dtoMes);
        }

        public override void OnExit(Server owner, string nextStateName)
        {
        }
    }
}
