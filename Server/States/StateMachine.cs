﻿using IOSockets.Commons;
using System;
using System.Collections.Generic;
using XmlParser.Dtos;

namespace Server.States
{
    /// <summary>
    /// Contain all states
    /// </summary>
    public class StateMachine
    {
        private Dictionary<string, StateBase> _states;
        private StateBase _currentState;
        private Server _owner;
        public string IdleStateName { get; private set; }
        public StateBase State { get { return _currentState; } }

        /// <summary>
        /// Create sate machine and register states
        /// </summary>
        /// <param name="owner">Server reference</param>
        /// <param name="idleStateName">Jobless state when nothing happens</param>
        public StateMachine(Server owner, string idleStateName)
        {
            _owner = owner;
            IdleStateName = idleStateName;
            _states = new Dictionary<string, StateBase>();
            InitializeBasicStates();
            ChangeState(this.IdleStateName, null, null);
        }

        /// <summary>
        /// Register all states supported by server
        /// </summary>
        private void InitializeBasicStates()
        {
            RegisterState(IdleStateName, new IdleState(this));
            RegisterState("playerLogin", new PlayerLoginState(this));
            RegisterState("gameMasterLogin", new GameMasterLoginState(this));
            RegisterState("beginGame", new BeginGameState(this));
            RegisterState("move", new MoveState(this));
            RegisterState("error", new ErrorState(this));
            RegisterState("logout", new LogoutState(this));
            RegisterState("thank you", new ThankYouState(this));
            RegisterState("leaveGame", new LeaveGameState(this));
            RegisterState("serverShutdown", new ServerShutdownState(this));
            RegisterState("connectionWithGMClosed", new ConnectionWithGMClosedState(this));
            RegisterState("gameState", new GameStateState(this));
            RegisterState("playerLeftGame", new PlayerLeftGameState(this));
            RegisterState("connectionWithPlayerClosed", new ConnectionWithPlayerClosedState(this));
        }

        /// <summary>
        /// Register new state in state machine
        /// </summary>
        /// <param name="name">Name of new state</param>
        /// <param name="state">State to register</param>
        public void RegisterState(string name, StateBase state)
        {
            if(state!=null)
                state.Name = name;
            _states.Add(name, state);
        }

        /// <summary>
        /// Change state to the new one
        /// </summary>
        /// <param name="name">Name of actual state</param>
        /// <param name="message">Contain connection to send next message</param>
        /// <param name="dtoMes">Deserialized message to hadle</param>
        public void ChangeState(string name, Message message, DtoMessage dtoMes)
        {
            if (!IsRegistered(name))
            {
                throw new ArgumentException(name);
            }
            string previousStateName = null;
            if (_currentState != null)
            {
                previousStateName = _currentState.Name;
                _currentState.OnExit(_owner, name);
            }
            _currentState = _states[name];
            if(_currentState!=null)
                _currentState.OnEnter(_owner, previousStateName, message, dtoMes);
        }

        /// <summary>
        /// Check if state is registered
        /// </summary>
        /// <param name="name">Name of state to check</param>
        /// <returns>True if contains, otherwise false</returns>
        public bool IsRegistered(string name)
        {
            if (_states.ContainsKey(name)) return true;
            else return false;
        }
    }
}
