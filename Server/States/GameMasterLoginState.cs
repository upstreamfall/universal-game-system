﻿using IOSockets.Commons;
using System;
using XmlParser.Dtos;

namespace Server.States
{
    /// <summary>
    /// State to handle message with type 'gameMasterLogin'
    /// </summary>
    public class GameMasterLoginState : StateBase
    {
        /// <summary>
        /// Create new state
        /// </summary>
        /// <param name="stateMachine">reference to the state machine</param>
        public GameMasterLoginState(StateMachine stateMachine) : base(stateMachine) { }

        /// <summary>
        /// Register new game master and check possibility to start new game
        /// </summary>
        /// <param name="owner">Server object to make changes in managers</param>
        /// <param name="previousStateName">Previous state of state machine</param>
        /// <param name="message">Contain connection to send next message</param>
        /// <param name="dtoMes">Deserialized message to handle in this state</param>
        public override void OnEnter(Server owner, string previousStateName, Message message, XmlParser.Dtos.DtoMessage dtoMes)
        {
            if (!(dtoMes is DtoGameMasterLogin)) 
                throw new ArgumentException("Wrong Dto type in OnEnter event function");
            DtoGameMasterLogin gmLogin = dtoMes as DtoGameMasterLogin;
            if (gmLogin.GameType.Equals("") || null == gmLogin.GameType)
            {
                log.Warn("Player game type is empty!");
                string response = owner.XmlParser.Serialize(new DtoLoginResponse("no", (int)LoginResponseError.Improper_game_type));
                log.Debug("Sending LoginResponse:\n" + response + "\n");
                message.connection.SendMsg(response);
                this._stateMachine.ChangeState(this._stateMachine.IdleStateName, message, dtoMes);
            }
            else if (HandleGameMasterLoginErrors(owner, gmLogin, message.connection))
            {
                owner.GameMastersMan.AddGameMaster(gmLogin, message.connection);
                log.Info("Game master with id=" + gmLogin.Id + " logged in successfully!");
                string response = owner.XmlParser.Serialize(new DtoLoginResponse("yes"));
                log.Debug("Sending LoginResponse:\n" + response + "\n");
                message.connection.SendMsg(response);                
                this._stateMachine.ChangeState("beginGame", message, dtoMes);
            }
            else this._stateMachine.ChangeState(this._stateMachine.IdleStateName, message, dtoMes);
        }

        public override void OnExit(Server owner, string nextStateName)
        {
        }

        private bool HandleGameMasterLoginErrors(Server owner, DtoGameMasterLogin gmLogin, Connection connection)
        {
            int error = (int)LoginResponseError.Empty;
            if (owner.GameMastersMan.IsGMRegistered(gmLogin.Id))
                error = (int)LoginResponseError.Improper_game_type;
            else if (gmLogin.PlayersMax < gmLogin.PlayersMin || gmLogin.PlayersMin <= 1)
                error = (int)LoginResponseError.Wrong_game_type_description_data;
            if (error!=0)
            {
                log.Warn("Can't add GameMaster with id = " + gmLogin.Id + " error nr. " + error.ToString() + " occured!");
                string response = owner.XmlParser.Serialize(new DtoLoginResponse("no", error));
                log.Debug("Sending LoginResponse:\n" + response + "\n");
                connection.SendMsg(response);
                return false;
            }
            return true;
        }
    }
}
