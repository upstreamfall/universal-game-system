﻿
namespace Server.States
{

    public class ChampionsListState : StateBase
    {
        public ChampionsListState(StateMachine stateMachine) : base(stateMachine) { }

        public override void OnEnter(Server owner, string previousStateName, IOSockets.Commons.Message message, XmlParser.Dtos.DtoMessage dtoMes)
        {
        }

        public override void OnExit(Server owner, string nextStateName)
        {
        }
    }
}
