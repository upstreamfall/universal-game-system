﻿using XmlParser.Dtos;

namespace Server.States
{
    /// <summary>
    /// State to handle message with type 'shutdown' from server
    /// </summary>
    public class ServerShutdownState : StateBase
    {
        /// <summary>
        /// Create new instance
        /// </summary>
        /// <param name="stateMachine">reference to the state machine</param>
        public ServerShutdownState(StateMachine stateMachine) : base(stateMachine) { }

        /// <summary>
        /// Close all connection and shutdown server
        /// </summary>
        /// <param name="owner">Server object to make changes in managers</param>
        /// <param name="previousStateName">Previous state of state machine</param>
        /// <param name="message">Contain connection to send next message</param>
        /// <param name="dtoMes">Deserialized message to handle in this state</param>
        public override void OnEnter(Server owner, string previousStateName, IOSockets.Commons.Message message, XmlParser.Dtos.DtoMessage dtoMes)
        {
            string mes = owner.XmlParser.Serialize(new DtoServerShutdown());
            owner.GameMastersMan.SendMessageToAllGameMasters(mes);
            owner.PlayersMan.SendMessageToAllPlayers(mes);
            this._stateMachine.ChangeState(this._stateMachine.IdleStateName, null, null);
        }

        public override void OnExit(Server owner, string nextStateName)
        {
        }
    }
}
