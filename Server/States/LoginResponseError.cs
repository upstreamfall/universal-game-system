﻿
namespace Server.States
{
    /// <summary>
    /// Enumerator to send error in login response
    /// </summary>
    public enum LoginResponseError
    {
        Empty = 0,
        Wrong_nick,
        Improper_game_type,
        Player_pool_overflow,
        Master_for_this_game_already_registered,
        Wrong_game_type_description_data
    }
}
