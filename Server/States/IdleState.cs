﻿
namespace Server.States
{
    /// <summary>
    /// State of jobless server
    /// </summary>
    public class IdleState : StateBase
    {
        /// <summary>
        /// Create new state
        /// </summary>
        /// <param name="stateMachine">reference to the state machine</param>
        public IdleState(StateMachine stateMachine) : base(stateMachine) { }

        /// <summary>
        /// Do nothing
        /// </summary>
        /// <param name="owner">Server object to make changes in managers</param>
        /// <param name="previousStateName">Previous state of state machine</param>
        /// <param name="message">Contain connection to send next message</param>
        /// <param name="dtoMes">Deserialized message to handle in this state</param>
        public override void OnEnter(Server owner, string previousStateName, IOSockets.Commons.Message message, XmlParser.Dtos.DtoMessage dtoMes)
        {
            if(previousStateName != null)
                log.Debug("Processing "+previousStateName+" state compleated");
            if (message != null && message.message != null)
                log.Debug("Ukończono przetwarzanie wiadomości:\n" + message);
        }

        public override void OnExit(Server owner, string nextStateName)
        {
            log.Debug("Exiting idle state and starting "+nextStateName+" state");
        }
    }
}
