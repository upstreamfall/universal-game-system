﻿using System.Collections.Generic;

namespace Server.States
{
    /// <summary>
    /// State to handle connection closing from game master
    /// </summary>
    public class ConnectionWithGMClosedState : StateBase
    {
        /// <summary>
        /// Create new state
        /// </summary>
        /// <param name="stateMachine">reference to the state machine</param>
        public ConnectionWithGMClosedState(StateMachine stateMachine) : base(stateMachine) { }

        /// <summary>
        /// Send all player connected to game master information
        /// </summary>
        /// <param name="owner">Server object to make changes in managers</param>
        /// <param name="previousStateName">Previous state of state machine</param>
        /// <param name="message">Contain connection to send next message</param>
        /// <param name="dtoMes">Deserialized message to handle in this state</param>
        public override void OnEnter(Server owner, string previousStateName, IOSockets.Commons.Message message, XmlParser.Dtos.DtoMessage dtoMes)
        {
            string gmId = owner.GameMastersMan.GetGMIdByConnection(message.connection);
            List<string> gamesRemoved = owner.GamesMan.RemoveGameMastersGames(gmId);
            log.Debug("Connection problem with GM - removing GM games");
            foreach (string game in gamesRemoved)
                owner.PlayersMan.KickPlayersFromGame(game);
            log.Debug("Removing players from games provided by problematic GM");
            owner.GameMastersMan.RemoveGameMaster(gmId);
            log.Warn("Connection with Game master (id = " + gmId + ") closed! (GM removed)");
            this._stateMachine.ChangeState(this._stateMachine.IdleStateName, message, dtoMes);
        }

        public override void OnExit(Server owner, string nextStateName)
        {
        }
    }
}
