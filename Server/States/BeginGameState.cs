﻿using IOSockets.Commons;
using System.Collections.Generic;
using XmlParser.Dtos;

namespace Server.States
{
    /// <summary>
    /// State to handle message with type 'beginGame'
    /// </summary>
    public class BeginGameState : StateBase
    {
        /// <summary>
        /// Create new state
        /// </summary>
        /// <param name="stateMachine">reference to the state machine</param>
        public BeginGameState(StateMachine stateMachine) : base(stateMachine) { }

        /// <summary>
        /// Collect players to new game and send to game master
        /// </summary>
        /// <param name="owner">Server object to make changes in managers</param>
        /// <param name="previousStateName">Previous state of state machine</param>
        /// <param name="message">Contain connection to send next message</param>
        /// <param name="dtoMes">Deserialized message to handle in this state</param>
        public override void OnEnter(Server owner, string previousStateName, Message message, XmlParser.Dtos.DtoMessage dtoMes)
        {
            Game newGame = new Game();
            string gameType = dtoMes is DtoGameMasterLogin ? (dtoMes as DtoGameMasterLogin).GameType : (dtoMes as DtoPlayerLogin).GameType;
            if (owner.GamesMan.SearchForNewGame(gameType, owner.GameMastersMan, owner.PlayersMan, out newGame))
            {
                owner.GamesMan.AddNewGame(newGame);
                log.Info("Serwer utworzył grę o id = " + newGame.Id + " z GM o id = " + newGame.gameMaster.Id);
                DtoBeginGame dtoBeginGame = new DtoBeginGame();
                dtoBeginGame.GameId = newGame.Id;
                dtoBeginGame.Players = new List<DtoPlayer>();
                for (int i = 0; i < newGame.players.Count; ++i)
                {
                    DtoPlayer player = new DtoPlayer(newGame.players[i].Nick);
                    dtoBeginGame.Players.Add(player);
                }
                string beginGameMes = owner.XmlParser.Serialize(dtoBeginGame);
                log.Debug("Serwer rozpoczyna grę wysyłając do GM o id = " + newGame.gameMaster.Id + " wiadomość:\n"+beginGameMes);
                owner.GameMastersMan.SendMessageToGM(beginGameMes, newGame.gameMaster.Id);
            }
            this._stateMachine.ChangeState(this._stateMachine.IdleStateName, message, dtoMes);
        }

        public override void OnExit(Server owner, string nextStateName)
        {
        }
    }
}
