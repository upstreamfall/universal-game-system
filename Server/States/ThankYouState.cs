﻿using System;
using XmlParser.Dtos;

namespace Server.States
{
    /// <summary>
    /// State to handle message with type 'thankYou' after finish game
    /// </summary>
    public class ThankYouState : StateBase
    {
        /// <summary>
        /// Create new state
        /// </summary>
        /// <param name="stateMachine">reference to the state machine</param>
        public ThankYouState(StateMachine stateMachine) : base(stateMachine) { }

        /// <summary>
        /// Print on screen 'thank you' 
        /// </summary>
        /// <param name="owner">Server object to make changes in managers</param>
        /// <param name="previousStateName">Previous state of state machine</param>
        /// <param name="message">Contain connection to send next message</param>
        /// <param name="dtoMes">Deserialized message to handle in this state</param>
        public override void OnEnter(Server owner, string previousStateName, IOSockets.Commons.Message message, XmlParser.Dtos.DtoMessage dtoMes)
        {
            if (!(dtoMes is DtoThankyou)) throw new ArgumentException("Wrong Dto type in OnEnter event function");
            string nick = string.Empty;
            if (this.CheckPlayerRegistration(owner, message))
            {
                string info = "Server received \"thank you\" ";
                info += "(gameId = " + (dtoMes as DtoThankyou).GameId + ") ";
                if ((nick = owner.PlayersMan.GetPlayerNick(message.connection)) != string.Empty)
                    info += "from " + nick;
                log.Info(info);
            }
            this._stateMachine.ChangeState(this._stateMachine.IdleStateName, message, dtoMes);
        }

        public override void OnExit(Server owner, string nextStateName)
        {
        }
    }
}
