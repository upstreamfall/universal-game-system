﻿using System;
namespace Server
{
    /// <summary>
    /// Interface implements protocol for every game type for game manager
    /// </summary>
    interface IGamesManager
    {
        /// <summary>
        /// Add new game to container
        /// </summary>
        /// <param name="newGame">Game waiting for adding</param>
        void AddNewGame(Game newGame);
        
        /// <summary>
        /// Return game master given game 
        /// </summary>
        /// <param name="gameId">Id to search game master</param>
        /// <returns>Game master if exist otherwise null</returns>
        GameMaster GetGameMasterFromGame(string gameId);
        
        /// <summary>
        /// Return game master Id from game with Id given as argument gameId
        /// </summary>
        /// <param name="gameId">Id of game</param>
        /// <returns>Game master Id if found otherwise empty string</returns>
        string GetGameMasterIdFromGame(string gameId);

        /// <summary>
        /// Return list of players from game 
        /// </summary>
        /// <param name="gameId">Id of game</param>
        /// <returns>List of players if game with gameId exists</returns>
        System.Collections.Generic.List<Player> GetPlayersFromGame(string gameId);

        /// <summary>
        /// Remove all games with given game master
        /// </summary>
        /// <param name="gmId">Gama master's Id</param>
        /// <returns>List of all game manages by game master</returns>
        System.Collections.Generic.List<string> RemoveGameMastersGames(string gmId);

        /// <summary>
        /// Remove one player from game
        /// </summary>
        /// <param name="player">Object of player to remove</param>
        void RemovePlayerFromGame(Player player);

        /// <summary>
        /// Check if new game can start
        /// </summary>
        /// <param name="gameType">type of game to start</param>
        /// <param name="gameMasterManager">container of game master to manage game</param>
        /// <param name="playerManager">container of players to play game</param>
        /// <param name="newGame">game to start</param>
        /// <returns>True if can start new game, otherwise false</returns>
        bool SearchForNewGame(string gameType, GameMastersManager gameMasterManager, PlayersManager playerManager, out Game newGame);
    }
}
