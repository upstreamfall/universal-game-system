﻿using IOSockets.Commons;
using System.Collections.Generic;
using System.Linq;
using XmlParser.Dtos;

namespace Server
{
    /// <summary>
    /// Container for player properties
    /// </summary>
    public class Player
    {
        public string GameType;
        public string Nick;
        public bool isPlaying;
        public string GameId;
        public Connection Connection;
    }

    /// <summary>
    /// Manage all players registered in server
    /// </summary>
    public class PlayersManager : IPlayersManager
    {
        private int _maxPlayerInPool;
        private Dictionary<string, Player> _players;
        private Dictionary<Connection, string> _playerConnections;
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Create new player manager and create new dictionary as a container
        /// </summary>
        public PlayersManager()
        {
            _maxPlayerInPool = 1000;
            _players = new Dictionary<string, Player>();
            _playerConnections = new Dictionary<Connection, string>();
        }

        public bool AddPlayer(DtoPlayerLogin player, Connection connection)
        {
            if (_players.Count < _maxPlayerInPool)
            {
                Player newPlayer = new Player();
                newPlayer.GameType = player.GameType;
                newPlayer.Nick = player.Nick;
                newPlayer.isPlaying = false;
                newPlayer.Connection = connection;
                newPlayer.GameId = string.Empty;
                _players.Add(newPlayer.Nick, newPlayer);
                _playerConnections.Add(connection, player.Nick);

                return true;
            }
            else return false;
        }

        public void RemovePlayer(string nick)
        {
            _playerConnections.Remove(_players[nick].Connection);
            _players.Remove(nick);
        }

        public void RemovePlayersWithGameType(string GameType)
        {
            foreach (KeyValuePair<string, Player> pair in _players.Where(t => t.Value.GameType == GameType))
            {
                RemovePlayer(pair.Key);
            }
        }

        public void RemovePlayersRange(IList<Player> players)
        {
            foreach (Player p in players)
                RemovePlayer(p.Nick);
        }

        public void SendMessageToAllPlayers(string message)
        {
            foreach (Player p in _players.Values)
                p.Connection.SendMsg(message);
        }

        public void SendMessageToPlayer(string message, string nick)
        {
            if (_players.ContainsKey(nick))
                _players[nick].Connection.SendMsg(message);
        }

        public bool IsPlayerRegistered(Player player)
        {
            if (_players.ContainsValue(player)) return true;
            else return false;
        }

        public bool IsPlayerRegistered(string nick)
        {
            if (_players.ContainsKey(nick)) return true;
            else return false;
        }

        public bool IsPlayerRegistered(Connection connection)
        {
            if (_playerConnections.ContainsKey(connection))
                return true;
            else return false;
        }

        public Player GetPlayerByConnection(Connection connection)
        {
            return _players[_playerConnections[connection]];
        }

        public List<Player> FindPlayersWithGameType(string gameType, int pMin, int pMax)
        {
            List<Player> players = new List<Player>();
            players = _players.Values.Where(t => t.isPlaying == false && t.GameType == gameType).ToList();
            if (players.Count < pMin) return null;
            else if (players.Count > pMax) return players.Take(pMax).ToList();
            else return players;
        }

        public string GetPlayerNick(Connection connection)
        {
            if (_playerConnections.ContainsKey(connection))
                return _playerConnections[connection];
            else return string.Empty;
        }

        public Player GetPlayerByNick(string nick)
        {
            return _players[nick];
        }

        public void PlayerLeaveGame(string nick)
        {
            if (_players.ContainsKey(nick))
            {
                _players[nick].isPlaying = false;
                _players[nick].GameId = string.Empty;
            }
        }

        public void KickPlayersFromGame(string GameId)
        {
            List<Player> playersToKick = _players.Where(t => t.Value.GameId == GameId).Select(t => t.Value).ToList();
            foreach (Player p in playersToKick)
            {
                p.isPlaying = false;
                p.GameId = string.Empty;
            }
        }
    }
}
