﻿using IOSockets.Commons;
using IOSockets.ServerSockets;
using Server.States;
using System;
using XmlParser;
using XmlParser.Dtos;

namespace Server
{
    /// <summary>
    /// Class contains managers to forward messages between players and game masters
    /// </summary>
    public class Server
    {
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string _serverIP;
        private short _port;
        private Parser _xmlParser;
        private ServerSocket _sSocket;
        private StateMachine _stateMachine;

        /// <summary>
        /// Manager contains all connected players
        /// </summary>
        public PlayersManager PlayersMan { get; private set; }

        /// <summary>
        /// Manager contains all connected game masters
        /// </summary>
        public GameMastersManager GameMastersMan { get; private set; }

        /// <summary>
        /// Manager contains all running games
        /// </summary>
        public GamesManager GamesMan { get; private set; } 

        /// <summary>
        /// Parser to serialize and deserialize messages
        /// </summary>
        public Parser XmlParser { get { return _xmlParser; } }

        /// <summary>
        /// Create server in right ip and number of port
        /// </summary>
        /// <param name="ip">address to create server</param>
        /// <param name="port">number of port for client connection</param>
        public Server(string ip, short port)
        {
            _serverIP = ip;
            _port = port;
            Initialize();
        }

        private void Initialize()
        {
            _xmlParser = new Parser();           
            PlayersMan = new PlayersManager();
            GameMastersMan = new GameMastersManager();
            GamesMan = new GamesManager();
            _stateMachine = new StateMachine(this, "idleState");
            _sSocket = new ServerSocket(
                _serverIP,
                _port, 
                new ServerSocketCallbacks(MessageReceivedHandler, ClosedConnectionHandler));
        }

        private void MessageReceivedHandler(Message message)
        {
            DtoMessage dtoMes = null;
            try
            {
                dtoMes = (DtoMessage)_xmlParser.Deserialize(message.message);
                if (dtoMes != null)
                    _stateMachine.ChangeState(dtoMes.Type, message, dtoMes);
            }
            catch (ParserException ex)
            {
                log.Warn("Parser cannot deserialize message:\n" + message.message, ex);
            }
            catch (ArgumentException ex)
            {
                log.Error("State with name = \"" + ex.Message + "\" is not registered!");
                log.Debug("Message = \n" + message.message);
            }
        }

        private void ClosedConnectionHandler(Connection c)
        {
            if (this.PlayersMan.IsPlayerRegistered(c))
            {
                this._stateMachine.ChangeState("connectionWithPlayerClosed", new Message(c, string.Empty), new DtoLogout());
            }
            else if (this.GameMastersMan.IsGMRegistered(c))
            {
                this._stateMachine.ChangeState("connectionWithGMClosed", new Message(c, string.Empty), new DtoMessage());
            }
            else log.Warn("Unknown connection closed!!");
        }

        /// <summary>
        /// End server work
        /// </summary>
        public void Close()
        {
            log.Info("Server shutting down!");
            _stateMachine.ChangeState("serverShutdown", null, null);
            ServerSocket.CloseSocket();
        }
    }
}
