﻿using System.Collections.Generic;
using System.Linq;

namespace Server
{
    /// <summary>
    /// Container for game properties
    /// </summary>
    public class Game
    {
        public string Id;
        public GameMaster gameMaster;
        public List<Player> players;

        /// <summary>
        /// Return type of game
        /// </summary>
        /// <returns>type of game that it contains</returns>
        public string GameType()
        {
            return gameMaster.GameType;
        }
    }

    /// <summary>
    /// Manage all games contained in server
    /// </summary>
    public class GamesManager : IGamesManager
    {
        private Dictionary<string, Game> _games;
        private int _lastId;
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Create new games manager and set last ID for 0
        /// </summary>
        public GamesManager()
        {
            _lastId = 0;
            _games = new Dictionary<string, Game>();
        }

        public void AddNewGame(Game newGame)
        {
            _games.Add(newGame.Id, newGame);
        }

        public bool SearchForNewGame(string gameType, GameMastersManager gameMasterManager, PlayersManager playerManager, out Game newGame)
        {
            newGame = new Game();
            GameMaster gameMaster;
            List<Player> players;

            if (gameMasterManager.FindGameMasterWithGameType(gameType, out gameMaster))
            {
                players = playerManager.FindPlayersWithGameType(gameType, gameMaster.PlayersMin, gameMaster.PlayersMax);
                if (null != players)
                {
                    string id = (_lastId++).ToString();
                    newGame.players = players;
                    newGame.gameMaster = gameMaster;
                    newGame.Id = id;
                    players.ForEach(t => t.GameId = id);
                    return true;
                }
            }
            return false;
        }

        public List<Player> GetPlayersFromGame(string gameId)
        {
            if (_games.ContainsKey(gameId))
                return _games[gameId].players;
            else return null;
        }

        public string GetGameMasterIdFromGame(string gameId)
        {
            if (_games.ContainsKey(gameId))
                return _games[gameId].gameMaster.Id;
            else return string.Empty;
        }

        public GameMaster GetGameMasterFromGame(string gameId)
        {
            if (_games.ContainsKey(gameId))
                return _games[gameId].gameMaster;
            else return null;
        }

        public void RemovePlayerFromGame(Player player)
        {
            if (_games.ContainsKey(player.GameId))
                _games[player.GameId].players.Remove(player);
        }

        public List<string> RemoveGameMastersGames(string gmId)
        {
            List<string> gamesToRemove = _games.Where(t => t.Value.gameMaster.Id == gmId).Select(t => t.Key).ToList();
            foreach (string gameId in gamesToRemove)
            {
                _games.Remove(gameId);
            }
            return gamesToRemove;
        }

        public void RemoveGame(string gameId)
        {
            _games.Remove(gameId);
        }
    }
}
