﻿using System;
namespace Server
{
    /// <summary>
    /// Interface implements protocol for every game type for game master manager
    /// </summary>
    interface IGameMastersManager
    {
        /// <summary>
        /// Add game master with given arguments
        /// </summary>
        /// <param name="gameMasterLoginDto">Dto contains parameters to create game master</param>
        /// <param name="connection">Socket to connect with client</param>
        void AddGameMaster(XmlParser.Dtos.DtoGameMasterLogin gameMasterLoginDto, IOSockets.Commons.Connection connection);
        
        /// <summary>
        /// Create game master from given dto's parameters
        /// </summary>
        /// <param name="gameMasterLoginDto">Dto object contains game master's parameters </param>
        /// <returns>New instance of game master</returns>
        GameMaster CreateGameMasterFromDto(XmlParser.Dtos.DtoGameMasterLogin gameMasterLoginDto);

        /// <summary>
        /// Create game master from given dto's parameters
        /// </summary>
        /// <param name="gameMasterLoginDto">Dto object contains game master's parameters </param>
        /// <param name="connection">socket to connect with client</param>
        /// <returns>New instance of game master</returns>
        GameMaster CreateGameMasterFromDto(XmlParser.Dtos.DtoGameMasterLogin gameMasterLoginDto, IOSockets.Commons.Connection connection);

        /// <summary>
        /// Find any game master with given game type
        /// </summary>
        /// <param name="gameType">Search game type</param>
        /// <param name="gameMaster">Game master with right game type</param>
        /// <returns>True if game master found, otherwise false</returns>
        bool FindGameMasterWithGameType(string gameType, out GameMaster gameMaster);

        /// <summary>
        /// Return socket to connect with wanted game master
        /// </summary>
        /// <param name="gameMasterId">Game master Id, which socket we want</param>
        /// <param name="connection">Socket to connect with game master</param>
        /// <returns>True if found appropriate game master, otherwise false </returns>
        bool GetConnectionFromGM(string gameMasterId, out IOSockets.Commons.Connection connection);

        /// <summary>
        /// Return game master's Id assigned to socket
        /// </summary>
        /// <param name="connection">Socket to connecto to the client</param>
        /// <returns>Id of game master or string.Empty if not found</returns>
        string GetGMIdByConnection(IOSockets.Commons.Connection connection);

        /// <summary>
        /// Check if game master with socket is already registered
        /// </summary>
        /// <param name="connection">Socket to connect with game master</param>
        /// <returns>True if some registered otherwise false</returns>
        bool IsGMRegistered(IOSockets.Commons.Connection connection);

        /// <summary>
        /// Check if game master with socket is already registered
        /// </summary>
        /// <param name="gm">reference to game master what we want</param>
        /// <returns>True if some registered otherwise false</returns>
        bool IsGMRegistered(GameMaster gm);

        /// <summary>
        /// Check if game master with Id registered
        /// </summary>
        /// <param name="id">Id for search</param>
        /// <returns>True if some registered otherwise false</returns>
        bool IsGMRegistered(string id);

        /// <summary>
        /// Remove game master with Id as argument
        /// </summary>
        /// <param name="id">Id to find and remove</param>
        void RemoveGameMaster(string id);

        /// <summary>
        /// Send message to every game masters connected to server
        /// </summary>
        /// <param name="message">Message to send</param>
        void SendMessageToAllGameMasters(string message);

        /// <summary>
        /// Send message to game master with Id as argument
        /// </summary>
        /// <param name="message">Message to send</param>
        /// <param name="id">Id of game master</param>
        void SendMessageToGM(string message, string id);
    }
}
