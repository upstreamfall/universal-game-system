﻿using System;
[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace Server
{
    /// <summary>
    /// Main class to run server and check arguments
    /// </summary>
    class Program
    {
        private static readonly log4net.ILog log = 
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static Server server;

        static void Usage()
        {
            Console.WriteLine("Please try: ./Server --port PORT\n");
        }

        static void Main(string[] args)
        {
            if (args.Length != 2)
            {
                Usage();
                return;
            }
            if (!args[0].Equals("--port"))
            {
                Usage();
                return;
            }

            short port;

            if (!short.TryParse(args[1], out port))
            {
                Usage();
                return;
            }

            ServerInitialize("127.0.0.1", port);
        }

        private static void ServerInitialize(string ip, short port)
        {
            log.Info("Server starting with ip = "+ip.ToString()+" and port = "+port.ToString());
            Console.CancelKeyPress += Console_CancelKeyPress;
            server = new Server(ip, port);
        }

        static void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            server.Close();
        }
    }
}
