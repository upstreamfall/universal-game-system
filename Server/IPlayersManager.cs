﻿using System;
namespace Server
{
    /// <summary>
    /// Interface implements protocol for every game type for players manager
    /// </summary>
    interface IPlayersManager
    {
        /// <summary>
        /// Add new player to pool
        /// </summary>
        /// <param name="player">Player's object to add</param>
        /// <param name="connection">Socket to connect with client</param>
        /// <returns>True if can add new player</returns>
        bool AddPlayer(XmlParser.Dtos.DtoPlayerLogin player, IOSockets.Commons.Connection connection);

        /// <summary>
        /// Find list of player to start new game
        /// </summary>
        /// <param name="gameType">type of game to start</param>
        /// <param name="pMin">minimum number of players to start the game</param>
        /// <param name="pMax">maximum number of players in one game</param>
        /// <returns>List of players ready to play in new game</returns>
        System.Collections.Generic.List<Player> FindPlayersWithGameType(string gameType, int pMin, int pMax);

        /// <summary>
        /// Return player assigned to socket
        /// </summary>
        /// <param name="connection">Socket to connecto to the client</param>
        /// <returns>Player if found</returns>
        Player GetPlayerByConnection(IOSockets.Commons.Connection connection);

        /// <summary>
        /// Return player with nick given as argument
        /// </summary>
        /// <param name="nick">Nick of player</param>
        /// <returns>Player if found</returns>
        Player GetPlayerByNick(string nick);

        /// <summary>
        /// Return player nick assigned to socket
        /// </summary>
        /// <param name="connection">Socket to connecto to the client</param>
        /// <returns>Player's nick if exists</returns>
        string GetPlayerNick(IOSockets.Commons.Connection connection);

        /// <summary>
        /// Check if player with socket is already registered
        /// </summary>
        /// <param name="connection">Socket to connect with game master</param>
        /// <returns>True if some registered otherwise false</returns>
        bool IsPlayerRegistered(IOSockets.Commons.Connection connection);

        /// <summary>
        /// Check if player with socket is already registered
        /// </summary>
        /// <param name="player">reference to player what we want</param>
        /// <returns>True if some registered otherwise false</returns>
        bool IsPlayerRegistered(Player player);

        /// <summary>
        /// Check if player with nick registered
        /// </summary>
        /// <param name="nick">Nick for search</param>
        /// <returns>True if some registered otherwise false</returns>
        bool IsPlayerRegistered(string nick);

        /// <summary>
        /// Remove player from game
        /// </summary>
        /// <param name="gameId">Id of game</param>
        void KickPlayersFromGame(string gameId);

        /// <summary>
        /// Change player status to not playing
        /// </summary>
        /// <param name="nick">Nick of player</param>
        void PlayerLeaveGame(string nick);

        /// <summary>
        /// Remove player
        /// </summary>
        /// <param name="nick">Nick of player</param>
        void RemovePlayer(string nick);

        /// <summary>
        /// Remove all player from collection
        /// </summary>
        /// <param name="players">Collection of players</param>
        void RemovePlayersRange(System.Collections.Generic.IList<Player> players);

        /// <summary>
        /// Remove all player with game type given as argument
        /// </summary>
        /// <param name="GameType">type of game to remove</param>
        void RemovePlayersWithGameType(string GameType);

        /// <summary>
        /// Send message to all player
        /// </summary>
        /// <param name="message">Information to send</param>
        void SendMessageToAllPlayers(string message);

        /// <summary>
        /// Send message to player
        /// </summary>
        /// <param name="message">Message to send</param>
        /// <param name="nick">Nicj of player</param>
        void SendMessageToPlayer(string message, string nick);
    }
}
