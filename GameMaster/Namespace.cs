namespace GameMaster
{
    /// <summary>
    /// The <see cref="GameMaster"/> namespace contains classes for game master, which connects to server and manages games in one type.
    /// </summary>

    [System.Runtime.CompilerServices.CompilerGenerated]
    class NamespaceDoc
    {
    }
}
