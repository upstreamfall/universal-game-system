﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using XmlParser.Dtos;

namespace GameMaster
{
    /// <summary>
    /// Class manages one game
    /// </summary>
    public class AIChecker: IAIChecker
    {
        #region FIELDS

        protected static readonly ILog logger = LogManager.GetLogger(typeof(AIChecker));

        private string _gameId;
        private List<DtoPlayer> _players;
        private static int BoardDim = 20;
        private int[,] _board;
        private int _lastMove;

        #endregion

        #region CONSTRUCTOR

        public AIChecker(string gameId)
        {
            _gameId = gameId;

            _board = new int[BoardDim, BoardDim];
            _lastMove = -1;
        }

        #endregion

        #region METHODS

        /// <summary>
        /// Prepare new game to start and send first move
        /// </summary>
        /// <param name="players">list of players in the game</param>
        public DtoMessage CreateNewGame(List<DtoPlayer> players)
        {
            _players = new List<DtoPlayer>(players);
            
            return FirstMove();
        }

        /// <summary>
        /// Check if move is correct
        /// </summary>
        /// <param name="move">player's move</param>
        /// <param name="response">gamestate for all containing move if it's correct</param>
        /// <returns></returns>
        public bool CheckMove(DtoMove move, out DtoMessage response)
        {
            response = new DtoMessage();

            if ( !(move is DtoMoveTicTacToe) )
            {
                logger.Warn("Argument 'move' is not a type 'DtoMoveTicTacToe'");
                return false;
            }
            if ((move as DtoMoveTicTacToe).Move.Y == 0)
            {
                int x = (move as DtoMoveTicTacToe).Move.X;
                if (0 == x)
                    response = GameOver();
                if (_board[x,0] == 0)
                {
                    _board[x,0] = (_lastMove % 2) == 0 ? 1 : -1;
                    response = NextMove(x);
                }
                else response = GameOver();

                return true;
            }
            return false;
        }

        /// <summary>
        /// Select player to start the game for first time
        /// </summary>
        public DtoMessage FirstMove()
        {
            string nextPlayer = _players[++_lastMove % _players.Count].Nick;
            DtoGameStateTicTacToe gameState = new DtoGameStateTicTacToe(_gameId, nextPlayer);
            return gameState;
        }

        /// <summary>
        /// Send player's move to rest of players
        /// </summary>
        /// <param name="x">position</param>
        public DtoMessage NextMove(int x)
        {
            string nextPlayer = _players[(_lastMove + 1) % _players.Count].Nick;
            Tac tac = new Tac(x, 0);
            DtoGameStateTicTacToe gameState = new DtoGameStateTicTacToe(_gameId, nextPlayer, tac);
            return gameState;
        }

        /// <summary>
        /// Exclude player from game and check its status
        /// </summary>
        /// <param name="playerNick">player's nick to remove from game</param>
        public void ExcludePlayer(string playerNick)
        {
            try
            {
                _players.Remove(_players.Where(t => t.Nick == playerNick).First());
                CheckGameStatus();
            }
            catch (Exception e)
            {
                logger.Warn("player not found!");
                logger.Debug(e.ToString());
            }
        }

        /// <summary>
        /// Check if game is ending
        /// </summary>
        public void CheckGameStatus()
        {
            if (_players.Count <= 1)
                GameOver();
        }

        /// <summary>
        /// First player allways wins in communication test
        /// </summary>
        /// <returns>Game state with result of game</returns>
        public DtoMessage GameOver()
        {
            List<PlayerResult> results = new List<PlayerResult>();
            results.Add(new PlayerResult(_players[0].Nick, "win"));
            results.Add(new PlayerResult(_players[1].Nick, "lose"));
            DtoGameStateTicTacToe gameState = new DtoGameStateTicTacToe(_gameId, results);

            return gameState;
        }

        #endregion
    }
}
