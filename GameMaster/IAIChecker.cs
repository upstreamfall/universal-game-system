﻿using XmlParser.Dtos;

namespace GameMaster
{
    /// <summary>
    /// Interface for all games, it contains universal methods to manage game
    /// </summary>
    interface IAIChecker
    {
        bool CheckMove(DtoMove move, out DtoMessage response);
        DtoMessage CreateNewGame(System.Collections.Generic.List<XmlParser.Dtos.DtoPlayer> players);
        void ExcludePlayer(string playerNick);
        DtoMessage FirstMove();
        DtoMessage NextMove(int x);
        void CheckGameStatus();
        DtoMessage GameOver();
    }
}
