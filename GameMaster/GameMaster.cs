﻿using IOSockets.ClientSockets;
using IOSockets.Commons;
using log4net;
using XmlParser;
using XmlParser.Dtos;

namespace GameMaster
{
    /// <summary>
    /// Class to manage message designed for gameMaster
    /// </summary>
    public class GameMaster
    {
        #region FIELDS

        protected static readonly ILog logger = LogManager.GetLogger(typeof(GamesManager));

        /// <summary>
        /// Object responsibles for send messages
        /// </summary>
        private ClientSocket _connectionSender;

        /// <summary>
        /// Object responsibles for receive messages
        /// </summary>
        private ClientSocketCallbacks  _connectionReceiver;

        /// <summary>
        /// Object for serialize and deserialize messages
        /// </summary>
        private Parser _parser;

        /// <summary>
        /// Container for games
        /// </summary>
        private GamesManager _gamesChecker;

        #endregion

        #region CONSTRUCTORS

        /// <summary>
        /// Create new game master with given ip and port, next try to login game type
        /// </summary>
        /// <param name="ip">Address to connect</param>
        /// <param name="port">Number of port to connect</param>
        public GameMaster(string ip, short port, string mod)
        {
            _connectionReceiver = new ClientSocketCallbacks(MsgReceivedHandler, ConnectionClosedHandler);
            _connectionSender = new ClientSocket(ip, port, _connectionReceiver);

            _parser = new Parser();
            _gamesChecker = new GamesManager("5-in-line-tic-tac-toe", this);

            logger.Info("Connection with server on IP:" + ip + "; Port:" + port + "; established");

            if ( mod.Equals("game"))
                LoginSimpleGame();
        }

        #endregion

        #region METHODS

        /// <summary>
        /// Handler for receive messages
        /// </summary>
        /// <param name="msg">Message that already receives</param>
        private void MsgReceivedHandler(Message msg)
        {
            DtoMessage dtoMessage = _parser.Deserialize(msg.message);
            logger.Debug("Receive: " + msg);

            switch (dtoMessage.Type)
            {
                case "loginResponse":
                    checkLoginResponse(dtoMessage);
                    break;
                case "beginGame":
                    BeginNewGame(dtoMessage);
                    break;
                case "move":

                    NextMove(msg);
                    break;
                case "playerLeftGame":
                    ExcludePlayerFromGame(dtoMessage);
                    break;
                case "serverShutdown":
                    CloseOnTheOccasionOfServerShutdown(dtoMessage);
                    break;
                case "error":
                    HandleError(dtoMessage);
                    break;
                default:
                    logger.Warn("Unhandled message:" + msg.message);
                    break;
            }
        }

        private void LoginSimpleGame()
        {
            int playersMin = 2;
            int playersMax = 2;
            DtoGameMasterLogin gameMasterLogin = new DtoGameMasterLogin("GM-Group04", _gamesChecker.GameType, playersMin, playersMax);

            SendMessage(gameMasterLogin);

            logger.Info("Send gameMasterLogin message to server:\n   " + gameMasterLogin.ToString());
        }

        private void checkLoginResponse(DtoMessage dtoMessage)
        {
            DtoLoginResponse loginResponse = (DtoLoginResponse)dtoMessage;

            if (loginResponse.Accept.Equals("yes"))
                logger.Info("Registered to server!\n   Now wait for players...");
            else
            {
                logger.Warn("Server rejected registration with error no. " + loginResponse.ErrorId);
                logger.Warn("Game Master will shutdown immedietaly!");
                Close();
            }
        }

        private void BeginNewGame(DtoMessage dtoMessage)
        {
            DtoBeginGame beginGameState = (DtoBeginGame)dtoMessage;
            logger.Info(beginGameState.ToString());
            _gamesChecker.NewGame(beginGameState);
        }

        private void NextMove(Message message)
        {
            DtoMoveTicTacToe move = (DtoMoveTicTacToe)_parser.Deserialize<DtoMoveTicTacToe>(message.message);
            logger.Info(move.ToString());
            _gamesChecker.NextMove(move);
        }

        private void ExcludePlayerFromGame(DtoMessage dtoMessage)
        {
            DtoPlayerLeftGame excludePLayerMessage = (DtoPlayerLeftGame)dtoMessage;
            logger.Info(excludePLayerMessage.ToString());
            _gamesChecker.ExcludePlayer(excludePLayerMessage);
        }

        /// <summary>
        /// Send message to server which game Master is connected
        /// </summary>
        /// <param name="messageToSend">Message to send to the server</param>
        internal void SendMessage(DtoMessage messageToSend)
        {
            _connectionSender.connection.SendMsg(_parser.Serialize(messageToSend));
            logger.Debug("Sent message: " + messageToSend.ToString());
        }

        private void HandleError(DtoMessage dtoMessage)
        {
            logger.Info("Receive error: " + (dtoMessage as DtoError).ErrorMessage);
        }

        private void ConnectionClosedHandler(Connection connection)
        {
            logger.Info("Connection with server closed! Game Master ends immediately!");
        }

        private void CloseOnTheOccasionOfServerShutdown(DtoMessage dtoMessage)
        {
            logger.Info("Game master is closing because of:\n" + dtoMessage.ToString());
            Close();
        }

        private void Close()
        {
            _connectionSender.connection.client.Close();
        }

        #endregion
    }
}
