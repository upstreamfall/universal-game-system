﻿using log4net;
using System;
using System.Linq;
using XmlParser.Dtos;

namespace GameMaster
{
    /// <summary>
    /// Class responsibles for run application 
    /// </summary>
    class Initializer
    {
        protected static readonly ILog logger = LogManager.GetLogger(typeof(Initializer));

        private static void Usage()
        {
            Console.WriteLine("Please try: ./gameMaster --connect_to IP:PORT\n");
            logger.Info("Invalid arguments in application start");
        }

        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();
            logger.Info("logger starts and is working correctly");

            if (args.Length != 2)
            {
                Usage();
                return;
            }
            if (!args[0].Equals("--connect_to") || !args[1].Contains(':'))
            {
                Usage();
                return;
            }

            String[] split = args[1].Split(':');
            String ip = split[0];
            short port;

            if (!short.TryParse(split[1], out port))
            {
                Usage();
                return;
            }

            Console.WriteLine("Choose mod to play:\n" +
                              "1. Normal Game\n" +
                              "2. Test Mod, only send fake messages and print received");
            string mod = Console.ReadLine();
            int choice;
            int.TryParse(mod, out choice);
            if ( 1 == choice)
            {
                GameMaster _gameMaster = GameMasterInitialize(ip, port, "game");
            }
            else
            {
                GameMaster _gameMaster = GameMasterInitialize(ip, port, "test");

                int number_of_operation;
                string line;
                while (true)
                {
                    Console.WriteLine("Send message of type:");
                    Console.WriteLine("1. gameMasterLogin");
                    Console.WriteLine("2. gameState- next move");
                    Console.WriteLine("3. gameState- game result");
                    Console.WriteLine("4. error message");

                    line = Console.ReadLine();
                    int.TryParse(line, out number_of_operation);
                    DtoMessage dto = null;
                    switch (number_of_operation)
                    {
                        case 1:
                            dto = new DtoGameMasterLogin("testGM", "5-in-line-tic-tac-toe", 2, 2);
                            break;
                        case 2:
                            dto = new DtoGameState("5-in-line-tic-tac-toe", "testPlayerNextToMove");
                            break;
                        case 3:
                            dto = new DtoGameState("5-in-line-tic-tac-toe",
                                                   new System.Collections.Generic.List<PlayerResult>() { 
                                               new PlayerResult("firstPlayer", "win"), 
                                               new PlayerResult("secondPlayer", "lose") });
                            break;
                        case 4:
                            dto = new DtoError("GM sent error message");
                            break;
                        default:
                            Console.WriteLine("You sent inappropriate number of operation, please try again...");
                            break;
                    }
                    if (null != dto) _gameMaster.SendMessage(dto);
                }
            }
        }

        /// <summary>
        /// Create game master with network configuration
        /// </summary>
        /// <param name="ip">IP of server</param>
        /// <param name="port">Number of port to connect to the server</param>
        private static GameMaster GameMasterInitialize(string ip, short port, string mod)
        {
            return new GameMaster(ip, port, mod);
        }
    }
}
