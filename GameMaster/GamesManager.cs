﻿using IOSockets.Commons;
using log4net;
using System;
using System.Collections.Generic;
using XmlParser.Dtos;

namespace GameMaster
{
    /// <summary>
    /// Manage game container and forward message to right aiChecker
    /// </summary>
    public class GamesManager
    {
        #region FIELDS

        protected static readonly ILog logger = LogManager.GetLogger(typeof(GamesManager));

        /// <summary>
        /// Name of game that class object managements  
        /// </summary>
        public string GameType { get; private set; }

        private GameMaster _gameMaster;
        private Dictionary<string, AIChecker> _games;

        #endregion

        #region CONSTRUCTOR

        /// <summary>
        /// Create new game manager
        /// </summary>
        /// <param name="gameType">game type that rules are implemented</param>
        /// <param name="gameMaster">reference to the game master which controls messages</param>
        public GamesManager(string gameName, GameMaster gameMaster)
        {
            GameType = gameName;
            _gameMaster = gameMaster;
            _games = new Dictionary<string, AIChecker>();

            logger.Debug("Games manager created");
        }

        #endregion

        #region METHODS

        /// <summary>
        /// Add new game to container if it's in right type
        /// </summary>
        /// <param name="beginGameState">object contains information about game id and list of players</param>
        public void NewGame(DtoBeginGame beginGameState)
        {
            string gameId = beginGameState.GameId;
            if (!_games.ContainsKey(gameId))
            {
                AIChecker newChecker = new AIChecker(gameId);
                DtoMessage firstMove = newChecker.CreateNewGame(beginGameState.Players);
                _games.Add(gameId, newChecker);

                SendMessage( firstMove);
            }
        }

        /// <summary>
        /// Reseive next move from player, send to AI checker
        /// </summary>
        /// <param name="move">player's move</param>
        public void NextMove(XmlParser.Dtos.DtoMoveTicTacToe move)
        {
            try
            {
                DtoMessage response;
                if (_games[move.GameId].CheckMove(move, out response))
                {
                    SendMessage(response);
                }
            }
            catch(KeyNotFoundException e)
            {
                logger.Warn("gameId not found!");
                logger.Debug(e.ToString());
            }
        }

        /// <summary>
        /// Exclude player from game
        /// </summary>
        /// <param name="excludePLayerMessage">player's id</param>
        public void ExcludePlayer(XmlParser.Dtos.DtoPlayerLeftGame excludePLayerMessage)
        {
            try
            {
                _games[excludePLayerMessage.GameId].ExcludePlayer(excludePLayerMessage.Nick);
            }
            catch (KeyNotFoundException e)
            {
                logger.Warn("gameId not found!");
                logger.Debug(e.ToString());
            }
        }

        /// <summary>
        /// Send
        /// </summary>
        /// <param name="message"></param>
        public void SendMessage(DtoMessage message)
        {
            _gameMaster.SendMessage(message);
        }

        #endregion
    }
}
