﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Configuration;
using System.Collections.Concurrent;

using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;

//  Protokół
//  [Header][Msg]
//  [Header] - 2 bajty kodujące długość nadsyłanych danych (n)
//  [Msg]   -  zserializowany string o n bajtach

namespace server.ServerSockets
{
    /// <summary>
    /// Ograniczenie liczby argumentów kontruktora ServerSocket
    /// </summary>
    public class ServerSocketCallbacks
    {
        public Action<Message> MesageReceived { get; private set; }
        public Action<Connection> ConnectionClosed { get; private set; }

        public ServerSocketCallbacks(Action<Message> _message_received_handler, Action<Connection> _connection_closed_handler)
        {
            MesageReceived = _message_received_handler;
            ConnectionClosed = _connection_closed_handler;
        }
    }


    /// <summary>
    /// Reprezentuje pojedyncze połączenie, referencja na ten obiekt jednoznacznie identyfikuje połączenie
    /// </summary>
    public class Connection{
        public Socket socket { get; private set; }
        public MessageQueue output { get; private set; }

        public Connection(Socket _socket)
        {
            socket = _socket;
            output = new MessageQueue();
        }

        /// <summary>
        /// Wysyła wiadomość do odbiorcy (kolejkuje)
        /// </summary>
        /// <param name="msg"></param>
        public void SendMsg(string msg)
        {
            output.Push(new Message(this, msg));
        }
    }

    public class ConnectionDescription
    {
        public Socket socket { get; private set; }
        public Thread input { get; private set; }
        public Thread output { get; private set; }

        public ConnectionDescription(Socket _socket, Thread _input, Thread _output)
        {
            socket = _socket;
            input = _input;
            output = _output;
        }
    }

    /// <summary>
    /// Reprezentuje odebraną wiadomość
    /// w kontekscie źrodła
    /// </summary>
    public class Message{
        public Connection connection{ get; private set; }
        public string message { get; private set; }

        public Message(Connection con, string msg){
            message = msg;
            connection = con;
        }
    }

    public class MessageQueueReleasedException : Exception
    {
        public MessageQueueReleasedException() { }
    }

    /// <summary>
    /// Bezpieczna (wielowatkowosc) kolejka wiadomości z blokowaniem na Pop
    /// </summary>
    public class MessageQueue{
        private Queue<Message> queue = new Queue<Message>();
        private Semaphore sem = new Semaphore(0, Int16.MaxValue);
        private bool released = false;

        public MessageQueue(){

        }

        public void Push(Message m){
            lock (queue)
            {
                queue.Enqueue(m);
                sem.Release(1);
            }
        }


        public void Pop(out Message m){
            sem.WaitOne();
            if (released)
            {
                throw new MessageQueueReleasedException();
            }
            lock (queue)
            {
                m = queue.Dequeue();
            }
        }

        public void Release()
        {
            released = true;
            sem.Release(1);
        }

    }

    /// <summary>
    /// Główna klasa obsługi połączeń
    /// Dlaczego nie singleton:
    ///     Można uruchomić osobne 'kanały' dla GM i graczy na różnych portach, z różnymi handlerami, przy czym trzeba bedzie synchronizować logike serwera
    /// </summary>
    public class ServerSocket
    {
        private static TcpListener listener;
        private static ServerSocketCallbacks callbacks;
        private static System.Threading.Thread listener_thread;
        private static System.Threading.Thread input_serializer_thread;

        private static MessageQueue input;

        private static ConcurrentDictionary<Connection, ConnectionDescription> connections = new ConcurrentDictionary<Connection, ConnectionDescription>();
        /// <summary>
        /// Konstruktor
        /// </summary>
        /// <param name="_ip_adress">Adres ip serwera</param>
        /// <param name="_port">Port nasłuchu</param>
        /// <param name="_callbacks">Zbiór callbacków - zdefiniowany przed uruchomieniem nasłuchu, niezmienny</param>
        public ServerSocket(string _ip_adress, short _port, ServerSocketCallbacks _callbacks) {
            listener = new TcpListener(IPAddress.Parse(_ip_adress), _port);
            listener.Start();
            callbacks = _callbacks;

            input = new MessageQueue();
            listener_thread = new Thread(ListenerThread);
            listener_thread.Start();

            input_serializer_thread = new Thread(InputHandler);
            input_serializer_thread.Start();
        }

        /// <summary>
        /// Pozawala wysłać wiadomosc do podanego odbiorcy
        /// </summary>
        /// <param name="con"></param>
        /// <param name="msg"></param>
        public void SendMsg(Connection con, string msg)
        {
            con.SendMsg(msg);
        }

        public static void CloseSocket()
        {

        }

        private void Kill(Connection c)
        {
            if(connections.ContainsKey(c)){
                ConnectionDescription cd;
                bool r = connections.TryGetValue(c, out cd);
                if (r)
                {
                    cd.socket.Close();
                    callbacks.ConnectionClosed(c);
                    c.output.Release();
                    cd.input.Abort();
                    cd.output.Abort();
                }
                else
                {
                    throw new Exception();
                }
            }
        }

        /// <summary>
        /// Synchronizuje wywołanie callback'u odebranej wiadomości
        /// Odebrane wiadomosci zbierane są z różnych wątków na wspólnej kolejce gdzie osobny watek wywołuje callback.
        /// </summary>
        private void InputHandler()
        {
            while (true)
            {
                Message msg;
                input.Pop(out msg);
                callbacks.MesageReceived(msg);
            }
        }

        /// <summary>
        /// Tryb pracy wątku odbiorczego
        /// </summary>
        enum Mode { Header, Message };
        
        /// <summary>
        /// Wątek odbierający dla danego połączenia
        /// </summary>
        /// <param name="o"></param>
        private void ConnectionInputHandler(Object o)
        {
            Connection con = (Connection)o;
            Stream s = new NetworkStream(con.socket);
            BinaryReader br = new BinaryReader(s);
            IFormatter formatter = new BinaryFormatter();

            byte[] data;
            Mode read_mode = Mode.Header;

            int data_size = 0;
            while (true)
            {
                switch (read_mode)
                {
                    case Mode.Header:
                    {
                        try
                        {
                            data = br.ReadBytes(2);
                        }
                        catch (Exception e)
                        {
                            Kill(con);
                            return;
                        }
                        data_size = ((int)data[0]) * 256 + (int)data[1];
                        read_mode = Mode.Message;
                        break;
                    }
                    case Mode.Message:
                    {
                        try{
                            data = br.ReadBytes(data_size);
                        }
                        catch (Exception e)
                        {
                            Kill(con);
                            return;
                        }
                        read_mode = Mode.Header;
                        MemoryStream ms = new MemoryStream(data);
                        string msg = (string)formatter.Deserialize(ms);
                        input.Push(new Message(con, msg));
                        break;
                    }
                }

            }
        }

        /// <summary>
        /// Wątek wysyłający dla danego połączenia
        /// </summary>
        /// <param name="o"></param>
        private void ConnectionOutputtHandler(Object o)
        {
            Connection con = (Connection)o;
            Stream s = new NetworkStream(con.socket);
            BinaryWriter bw = new BinaryWriter(s);

            IFormatter formatter = new BinaryFormatter();
            byte[] data;
            byte[] length = new byte[2];
            while (true)
            {
                Message msg;
                try
                {
                    con.output.Pop(out msg);
                }
                catch (MessageQueueReleasedException e)
                {
                    return;
                }

                MemoryStream ms = new MemoryStream();
                formatter.Serialize(ms, msg.message);

                data = ms.ToArray();
                length[0] = (byte)(((data.Length) >> 8) & 0xFF);
                length[1] = (byte)((data.Length) & 0xFF);

                try{
                    bw.Write(length);
                    bw.Write(data);
                }
                catch (Exception e)
                {
                    Kill(con);
                    return;
                }
            }
        }

        /// <summary>
        /// Wątech nasłuchu nadchodzących połączeń
        /// </summary>
        private void ListenerThread()
        {
            while (true)
            {
                Socket sck = listener.AcceptSocket();
                Connection con = new Connection(sck);

                ParameterizedThreadStart pts_in = new ParameterizedThreadStart(ConnectionInputHandler);
                ParameterizedThreadStart pts_out = new ParameterizedThreadStart(ConnectionOutputtHandler);

                Thread th_in = new Thread(pts_in);
                Thread th_out = new Thread(pts_out);

                ConnectionDescription cdescr = new ConnectionDescription(sck, th_in, th_out);
                connections.GetOrAdd(con, cdescr);

                th_in.Start(con);
                th_out.Start(con);

            }
        }
    }
}
