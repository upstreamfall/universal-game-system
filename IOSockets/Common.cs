﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace IOSockets.Commons
{
    public enum Mode { Header, Message };

    public class Connection
    {
        public MessageQueue output { get; private set; }

        public Connection()
        {
            output = new MessageQueue();
        }

        /// <summary>
        /// Wysyła wiadomość do odbiorcy (kolejkuje)
        /// </summary>
        /// <param name="msg"></param>
        public void SendMsg(string msg)
        {
            output.Push(new Message(this, msg));
        }
    }


    public class Message
    {
        public Connection connection { get; private set; }
        public string message { get; private set; }

        public Message(Connection con, string msg)
        {
            message = msg;
            connection = con;
        }
    }


    public class MessageQueueReleasedException : Exception
    {
        public MessageQueueReleasedException() { }
    }

    public class MessageQueue
    {
        private Queue<Message> queue = new Queue<Message>();
        private Semaphore sem = new Semaphore(0, Int16.MaxValue);
        private bool released = false;

        public MessageQueue()
        {

        }

        public void Push(Message m)
        {
            lock (queue)
            {
                queue.Enqueue(m);
                sem.Release(1);
            }
        }


        public void Pop(out Message m)
        {
            sem.WaitOne();
            if (released)
            {
                throw new MessageQueueReleasedException();
            }
            lock (queue)
            {
                m = queue.Dequeue();
            }
        }

        public void Release()
        {
            released = true;
            sem.Release(1);
        }

    }
}
