﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Configuration;
using System.Collections.Concurrent;


using IOSockets.Commons;
//  Protokół
//  [Header][Msg]
//  [Header] - 2 bajty kodujące długość nadsyłanych danych (n)
//  [Msg]   -  zserializowany string o n bajtach

namespace IOSockets.ServerSockets
{

    public class ServerConnection : Connection
    {
        public Socket socket { get; private set; }
        public ServerConnection(Socket _socket)
        {
            socket = _socket;
        }
    }

    public class ServerSocketCallbacks
    {
        public Action<Message> MesageReceived { get; private set; }
        public Action<Connection> ConnectionClosed { get; private set; }

        public ServerSocketCallbacks(Action<Message> _message_received_handler, Action<Connection> _connection_closed_handler)
        {
            MesageReceived = _message_received_handler;
            ConnectionClosed = _connection_closed_handler;
        }
    }


    public class ConnectionDescription
    {
        public Socket socket { get; private set; }
        public Thread input { get; private set; }
        public Thread output { get; private set; }

        public ConnectionDescription(Socket _socket, Thread _input, Thread _output)
        {
            socket = _socket;
            input = _input;
            output = _output;
        }
    }
   
    public class ServerSocket
    {
        private static TcpListener listener;
        private static ServerSocketCallbacks callbacks;
        private static System.Threading.Thread listener_thread;
        private static System.Threading.Thread input_serializer_thread;

        private static MessageQueue input;

        private static ConcurrentDictionary<Connection, ConnectionDescription> connections = new ConcurrentDictionary<Connection, ConnectionDescription>();


        public ServerSocket(string _ip_adress, short _port, ServerSocketCallbacks _callbacks) {
            listener = new TcpListener(IPAddress.Parse(_ip_adress), _port);
            listener.Start();
            callbacks = _callbacks;

            input = new MessageQueue();
            listener_thread = new Thread(ListenerThread);
            listener_thread.Start();

            input_serializer_thread = new Thread(InputHandler);
            input_serializer_thread.Start();
        }


        public void SendMsg(Connection con, string msg)
        {
            con.SendMsg(msg);
        }

        public static void CloseSocket()
        {
        }

        private void Kill(Connection c)
        {
            if(connections.ContainsKey(c)){
                ConnectionDescription cd;
                bool r = connections.TryGetValue(c, out cd);
                if (r)
                {
                    cd.socket.Close();
                    callbacks.ConnectionClosed(c);
                    c.output.Release();
                    cd.input.Abort();
                    cd.output.Abort();
                }
                else
                {
                    throw new Exception();
                }
            }
        }


        private void InputHandler()
        {
            while (true)
            {
                Message msg;
                input.Pop(out msg);
                callbacks.MesageReceived(msg);
            }
        }


        
        
        private void ConnectionInputHandler(Object o)
        {
            ServerConnection con = (ServerConnection)o;
            Stream s = new NetworkStream(con.socket);
            BinaryReader br = new BinaryReader(s);
            Encoding enc = new UTF8Encoding(false, true);

            byte[] data;
            Mode read_mode = Mode.Header;

            int data_size = 0;
            while (true)
            {
                switch (read_mode)
                {
                    case Mode.Header:
                    {
                        try
                        {
                            data = br.ReadBytes(2);
                        }
                        catch (Exception e)
                        {
                            Kill(con);
                            return;
                        }

                        if (data.Length < 2) continue;

                        data_size = ((int)data[0]) * 256 + (int)data[1];
                        read_mode = Mode.Message;
                        break;
                    }
                    case Mode.Message:
                    {
                        try{
                            data = br.ReadBytes(data_size);
                        }
                        catch (Exception e)
                        {
                            Kill(con);
                            return;
                        }
                        read_mode = Mode.Header;
                        string msg = enc.GetString(data);
                        input.Push(new Message(con, msg));
                        break;
                    }
                }

            }
        }

        private void ConnectionOutputtHandler(Object o)
        {
            ServerConnection con = (ServerConnection)o;
            Stream s = new NetworkStream(con.socket);
            BinaryWriter bw = new BinaryWriter(s);

            Encoding enc = new UTF8Encoding(false, true);
            byte[] data;
            byte[] length = new byte[2];
            while (true)
            {
                Message msg;
                try
                {
                    con.output.Pop(out msg);
                }
                catch (MessageQueueReleasedException e)
                {
                    return;
                }

                data = enc.GetBytes(msg.message);
                length[0] = (byte)(((data.Length) >> 8) & 0xFF);
                length[1] = (byte)((data.Length) & 0xFF);

                try{
                    bw.Write(length);
                    bw.Write(data);
                }
                catch (Exception e)
                {
                    Kill(con);
                    return;
                }
            }
        }


        private void ListenerThread()
        {
            while (true)
            {
                Socket sck = listener.AcceptSocket();
                ServerConnection con = new ServerConnection(sck);

                ParameterizedThreadStart pts_in = new ParameterizedThreadStart(ConnectionInputHandler);
                ParameterizedThreadStart pts_out = new ParameterizedThreadStart(ConnectionOutputtHandler);

                Thread th_in = new Thread(pts_in);
                Thread th_out = new Thread(pts_out);

                ConnectionDescription cdescr = new ConnectionDescription(sck, th_in, th_out);
                connections.GetOrAdd(con, cdescr);

                th_in.Start(con);
                th_out.Start(con);

            }
        }
    }
}
