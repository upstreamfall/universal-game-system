﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net.Sockets;
using System.Threading;

using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;

namespace client.ClientSockets
{
    public class ClientSocketCallbacks
    {
        public Action<Message> MesageReceived { get; private set; }
        public Action<Connection> ConnectionClosed { get; private set; }

        public ClientSocketCallbacks(Action<Message> _message_received_handler, Action<Connection> _connection_closed_handler)
        {
            MesageReceived = _message_received_handler;
            ConnectionClosed = _connection_closed_handler;
        }
    }

    public class Message
    {
        public Connection connection { get; private set; }
        public string message { get; private set; }

        public Message(Connection con, string msg)
        {
            message = msg;
            connection = con;
        }
    }

    public class Connection
    {
        public TcpClient client { get; private set; }
        public MessageQueue output { get; private set; }

        public Connection(TcpClient _client)
        {
            client = _client;
            output = new MessageQueue();
        }

        /// <summary>
        /// Wysyła wiadomość do odbiorcy (kolejkuje)
        /// </summary>
        /// <param name="msg"></param>
        public void SendMsg(string msg)
        {
            output.Push(new Message(this, msg));
        }
    }

    public class MessageQueueReleasedException : Exception
    {
        public MessageQueueReleasedException() { }
    }

    /// <summary>
    /// Bezpieczna (wielowatkowosc) kolejka wiadomości z blokowaniem na Pop
    /// </summary>
    public class MessageQueue
    {
        private Queue<Message> queue = new Queue<Message>();
        private Semaphore sem = new Semaphore(0, Int16.MaxValue);
        private bool released = false;

        public MessageQueue()
        {

        }

        public void Push(Message m)
        {
            lock (queue)
            {
                queue.Enqueue(m);
                sem.Release(1);
            }
        }


        public void Pop(out Message m)
        {
            sem.WaitOne();
            if (released)
            {
                throw new MessageQueueReleasedException();
            }
            lock (queue)
            {
                m = queue.Dequeue();
            }
        }

        public void Release()
        {
            released = true;
            sem.Release(1);
        }

    }

    class ClientSocketExeption : Exception
    {
        public string msg { get; private set; }
        public ClientSocketExeption(string _msg)
        {
            msg = _msg;
        }
    }

    class ClientSocket
    {
        private TcpClient client;
        private Thread input_handler_thread;
        private Thread output_handler_thread;
        private ClientSocketCallbacks callbacks;
        private Stream stream;

        public Connection connection {get; private set;}

        public ClientSocket(string _ip_adress, short port, ClientSocketCallbacks _callbacks)
        {
            callbacks = _callbacks;
            TcpClient client;
            try{
                client = new TcpClient(_ip_adress, port);
                stream = client.GetStream();
                connection = new Connection(client);
                input_handler_thread = new Thread(ConnectionInputHandler);
                output_handler_thread = new Thread(ConnectionOutputHandler);
                input_handler_thread.Start();
                output_handler_thread.Start();
            }
            catch (Exception e)
            {
                throw new ClientSocketExeption("Counldn't create new connection");
            }
        }

        private void Kill()
        {
            stream.Close();
            callbacks.ConnectionClosed(connection);
            connection.output.Release();
            input_handler_thread.Abort();
            output_handler_thread.Abort();
        }

        enum Mode { Header, Message };

        private void ConnectionInputHandler()
        {
            BinaryReader br = new BinaryReader(stream);

            IFormatter formatter = new BinaryFormatter();

            byte[] data;
            Mode read_mode = Mode.Header;

            int data_size = 0;
            while (true)
            {
                switch (read_mode)
                {
                    case Mode.Header:
                        {
                            try
                            {
                                data = br.ReadBytes(2);
                            }
                            catch (Exception e)
                            {
                                Kill();
                                return;
                            }
                            data_size = ((int)data[0]) * 256 + (int)data[1];
                            read_mode = Mode.Message;
                            break;
                        }
                    case Mode.Message:
                        {
                            try{
                            data = br.ReadBytes(data_size);
                            }
                            catch (Exception e)
                            {
                                Kill();
                                return;
                            }
                            read_mode = Mode.Header;
                            MemoryStream ms = new MemoryStream(data);
                            string msg = (string)formatter.Deserialize(ms);
                            callbacks.MesageReceived(new Message(connection, msg));
                            break;
                        }
                }

            }

        }

        private void ConnectionOutputHandler()
        {
            BinaryWriter bw = new BinaryWriter(stream);

            IFormatter formatter = new BinaryFormatter();
            byte[] data;
            byte[] length = new byte[2];
            while (true)
            {
                Message msg;
                try
                {
                    connection.output.Pop(out msg);
                }
                catch (MessageQueueReleasedException e)
                {
                    return;
                }
                MemoryStream ms = new MemoryStream();
                formatter.Serialize(ms, msg.message);

                data = ms.ToArray();
                length[0] = (byte)(((data.Length) >> 8) & 0xFF);
                length[1] = (byte)((data.Length) & 0xFF);

                try{
                    bw.Write(length);
                    bw.Write(data);
                }
                catch (Exception e)
                {
                    Kill();
                    return;
                }
            }
        }

    }
}
