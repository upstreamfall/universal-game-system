﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net.Sockets;
using System.Threading;

using IOSockets.Commons;

namespace IOSockets.ClientSockets
{
    public class ClientSocketCallbacks
    {
        public Action<Message> MesageReceived { get; private set; }
        public Action<Connection> ConnectionClosed { get; private set; }

        public ClientSocketCallbacks(Action<Message> _message_received_handler, Action<Connection> _connection_closed_handler)
        {
            MesageReceived = _message_received_handler;
            ConnectionClosed = _connection_closed_handler;
        }
    }

  
    public class ClientConnection : Connection
    {
        public TcpClient client { get; private set; }

        public ClientConnection(TcpClient _client)
        {
            client = _client;

        }
    }


    public class ClientSocketExeption : Exception
    {
        public string msg { get; private set; }
        public ClientSocketExeption(string _msg)
        {
            msg = _msg;
        }
    }

    public class ClientSocket
    {
        private TcpClient client;
        private Thread input_handler_thread;
        private Thread output_handler_thread;
        private ClientSocketCallbacks callbacks;
        private Stream stream;

        public ClientConnection connection { get; private set; }

        public ClientSocket(string _ip_adress, short port, ClientSocketCallbacks _callbacks)
        {
            callbacks = _callbacks;
            TcpClient client;
            try{
                client = new TcpClient(_ip_adress, port);
                stream = client.GetStream();
                connection = new ClientConnection(client);
                input_handler_thread = new Thread(ConnectionInputHandler);
                output_handler_thread = new Thread(ConnectionOutputHandler);
                input_handler_thread.Start();
                output_handler_thread.Start();
            }
            catch (Exception ex)
            {
                throw new ClientSocketExeption("Counldn't create new connection ex: "+ex.Message);
            }
        }

        private void Kill()
        {
            stream.Close();
            callbacks.ConnectionClosed(connection);
            connection.output.Release();
            input_handler_thread.Abort();
            output_handler_thread.Abort();
        }

        enum Mode { Header, Message };

        private void ConnectionInputHandler()
        {
            BinaryReader br = new BinaryReader(stream);

            Encoding enc = new UTF8Encoding(false, true);
            

            byte[] data;
            Mode read_mode = Mode.Header;

            int data_size = 0;
            while (true)
            {
                switch (read_mode)
                {
                    case Mode.Header:
                        {
                            try
                            {
                                data = br.ReadBytes(2);
                            }
                            catch (Exception e)
                            {
                                Kill();
                                return;
                            }
                            if (data.Length != 2) continue;
                            data_size = ((int)data[0]) * 256 + (int)data[1];
                            read_mode = Mode.Message;
                            break;
                        }
                    case Mode.Message:
                        {
                            try{
                            data = br.ReadBytes(data_size);
                            }
                            catch (Exception e)
                            {
                                Kill();
                                return;
                            }
                            read_mode = Mode.Header;
                            string msg = enc.GetString(data);
                            callbacks.MesageReceived(new Message(connection, msg));
                            break;
                        }
                }

            }

        }

        private void ConnectionOutputHandler()
        {
            BinaryWriter bw = new BinaryWriter(stream);

            Encoding enc = new UTF8Encoding(false, true);

            byte[] data;
            byte[] length = new byte[2];
            while (true)
            {
                Message msg;
                try
                {
                    connection.output.Pop(out msg);
                }
                catch (MessageQueueReleasedException e)
                {
                    return;
                }


                data = enc.GetBytes(msg.message);
                length[0] = (byte)(((data.Length) >> 8) & 0xFF);
                length[1] = (byte)((data.Length) & 0xFF);

                try{
                    bw.Write(length);
                    bw.Write(data);
                }
                catch (Exception e)
                {
                    Kill();
                    return;
                }
            }
        }

    }
}
